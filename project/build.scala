import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

	val scalaVersion = "2.10.3"

	lazy val typiCore = sbt.Project(
		id = "typi-core",
		base = file("typi-core"),
		settings = Defaults.defaultSettings ++ Seq(
			sbt.Keys.name := "LambdaInterpreter",
			sbt.Keys.version := "1.0",
			sbt.Keys.scalaVersion := scalaVersion,
			sbt.Keys.libraryDependencies ++= Seq("org.specs2" %% "specs2" % "2.3.7" % "test"),
			sbt.Keys.scalacOptions in Test ++= Seq("-Yrangepos"),
			sbt.Keys.resolvers ++= Seq("snapshots", "releases").map(Resolver.sonatypeRepo)
		)
	)
	
	val typiWebDependencies = Seq(
		jdbc,
		anorm
	) 
	
	lazy val typiWeb = play.Project(
		"typi-web",
		path = file("typi-web"),
		settings = Defaults.defaultSettings ++ play.Project.playScalaSettings ++ Seq(
			sbt.Keys.version := "1.0",
			sbt.Keys.libraryDependencies ++= Seq(jdbc, anorm)
		)
	).dependsOn(typiCore)

}