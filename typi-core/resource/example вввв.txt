assign xx = 10;
xx;
assign yy = (Lambda u:Unit.xx);
assign zz = (Lambda u:Unit.{a = xx, b = 10});
(yy unit);
zz unit;
assign xx = unit;
yy xx;
127;
assign zz = (Lambda u:Unit.yy u);
assign a = 15;
zz xx;
6755;
assign oo = (Lambda u:Unit->Nat.(u xx));
assign zz = (Lambda u:Unit.{a = oo yy, b = xx});
assign a = 20;
zz xx;