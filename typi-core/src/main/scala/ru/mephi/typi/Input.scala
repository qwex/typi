package ru.mephi.typi

import SimpleLambda.{TyArr, TyUnit, VarBind, Literal}

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 23.07.13
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
object Input {
  val input3 =
    "let if = Lambda x.x in " +
      "let true  = (Lambda x.(Lambda y.x)) in" +
      "let false = (Lambda x.(Lambda y.y)) in" +
      "(((if@true)@false)@true)"
  val input =
    "let If = Lambda x.x in let True = Lambda x.(x@y) in (True@If)"
  val input2 =
    "((Lambda x.(x@y))@(Lambda x.x))"
  val input4 =
    "if if true then true else false then (Lambda x.x) else (Lambda truey.(y@y))"
  val input5 =
    "let x = Lambda y:bool.(y@y) in (Lambda x:bool.x)@(z@x)"
  val input6 =
    "Lambda y:bool.y"
  val input7 =
    "Lambda y:bool->(bool->((bool->bool)->bool)).Lambda x:bool->bool.Lambda x:bool.y@x"
  val input8 =
    "x"
  val input9 =
    "let x = Lambda y:bool.y in x"
  val input10 =
    "true"
  val input11 =
    "unit"
  val input12 =
    "(Lambda x:Unit.x)@unit"
  val input13 =
    "{abc = (Lambda x:bool->bool.x),bbb = let x = {j = a,l = b} in x,ccd = x}"
  val input14 =
    "Lambda x:{a = bool, b = bool}.x"
  val input15 =
    "(Lambda x:Unit.{a = x, b = true})@unit"
  val input16 =
    "{a = true, b = false}._a"
  val input17 =
    "{b = false}._b"
  val input18 =
    "x@y"
  val input19 =
    "((Lambda x:bool.{b = x}) @ false)._b"
  val input20 =
    "if true then false else true"
  val input21 = "" +
    "((Lambda a:Unit.Lambda x:Unit->Unit.x@a)@x)@s"
  val ctx21 =
    List(Literal("x") -> VarBind(TyUnit),Literal("s")->VarBind(TyArr(TyUnit,TyUnit)))
  val input22 =
    "x @ y @ z"
  val input23 =
    "x @ (y @ z)"
  val input24 =
    "x y z"
  val input25 =
    "let or = Lambda x:bool.Lambda y:bool. if x then x else y in or false true"
  val input26 =
    "{b = true}"
  val input27 =
    "10"
  val input28 =
    "succ 10"
  val input29 =
    "iszero 0"
  val input30 =
    "letrec x:Unit = unit in x"
  val input31 =
    "letrec iseven: Nat->bool = " +
      " Lambda x:Nat.if iszero x then " +
      "         true" +
      " else if iszero (pred x) then false" +
      " else iseven (pred (pred x)) in iseven 2"
  val input32 =
    "letrec iseven : Nat->bool = unit in iseven 2"
  val input33 =
    "if (iszero 1) then false else true"
  val input34 =
    "letrec plus:Nat->(Nat->Nat) = Lambda x:Nat. Lambda y:Nat. if (iszero x) then y else plus (pred x) (succ y) in plus 1390 1 "
  val input35 =
    "(iszero x)"
  val input36 =
    "(pred x)"
  val input37 =
    "if (iszero x) then y else plus (pred x) (succ y)"
  val input38 =
    "if x then y else plus (pred x) (succ y)"
  val input39 =
    "if x then y else plus (pred x) (succ u)"
  val input40 =
    "{a = 11, bah = 9}._bah"
  val input41 =
    "(Lambda x:Nat.x) 10"
  val input42 =
    "(Lambda x:{a = Nat, b = Nat}.x._a) {a = 10, b = 9}"
  val input43 =
    "TLambda x:Nat.x"
  val input44 =
    "TLambda x:*.x"
  val input45 =
    "TLambda x:*.Lambda y:x.y"
  val input46 =
    "("+input45+")"+" "+"Nat"
  val input47 =
    "TLambda x:*.Lambda y:x->x.Lambda t:x.y t"
  val input48 =
    "(TLambda x:*.Lambda y:x->x.Lambda t:x.y t) Nat"
  val input49 =
    "(TLambda x:Nat.x)"
  val input50 =
    "(TLambda x:*.Lambda y:x.y)"
  val input51 =
    "(TLambda x:*.Lambda y:x->x.Lambda t:x.Lambda z:(x->x)->(x->x).z y t)"
  val input52 =
    input51 + " Unit"
  val input53 =
    "TLambda x:*.Lambda y:x.succ y"
  val input54 =
    "TLambda x:*.10"
  val input55 =
    "(TLambda x:*.10) Unit"
  val input56 =
    "(Lambda x:{a:Nat,b:Nat}.x._b) {a = 10, b = (Lambda x:Nat.x) (succ 11)}"
  val input57 =
    "(TLambda x:*.Lambda y:x.y) (Nat->Nat)"
  val input58 =
    "(TLambda x:*.x)"
  val input59 =
    "(if true then Nat else Unit)"
  val input60 =
    "(TLambda x:*.Lambda y:x.y) "+input59
  val input61 =
    "(TLambda x:*.Lambda y:x.y) Nat"
  val input62 =
    "cons[Nat,0] 10 nil[Nat]"
  val input63 =
    "cons[Nat,0](10,nil[Nat])"
  val input64 =
    "cons[Nat,1](20,cons[Nat,0](11,nil[Nat]))"
  val input65 =
    "TLambda x:*.nil[x]"
  val input66 =
    "Lambda x:Nat.cons[Nat,0](x,nil[Nat])"
  val input67 =
    "("+input66+")"+"13"
  val input68 =
    "(TLambda n:Nat.Lambda x:Vec(Nat,succ n).cons[Nat,succ succ n](23,cons[Nat,succ n](45,x))) 17"
  val input69 =
    "(TLambda x:*.Lambda u:Nat.TLambda y:*.Lambda t:x->y.t) Nat 10 Unit"
  val input70 =
    "(TLambda x:*.Lambda u:Nat.TLambda y:*.Lambda t:x->y.t)"
  val input71 =
    "(TLambda n:Nat.Lambda x:Vec(Nat,n).x) 14"
  val input72 =
    "(TLambda x:*.Lambda y:x.y)"
  val input73 =
    "(TLambda n:Nat.Lambda y:Vec(Nat,n).cons[Nat,n](45,y))"
  val input74 =
    "Pi n:*.(n->n)->n"
  val input75 =
    "(TLambda n:*.Lambda x:n.Lambda y:n->n.Lambda i:n.y i) Nat"
  val input76 =
    "(TLambda n:*.Lambda x:n.Lambda y:n->n.y x) Nat"
  val input77 =
    "(Lambda x:Nat.succ succ succ x)"
  val input78 =
    "(Lambda x:Unit.x)"
  val input79 =
    "(TLambda n:Nat.Lambda x:Vec(Nat,succ succ n).cons[Nat,succ succ n](10,x)) 10"
  val input80 =
    "(TLambda n:Nat.Vec(Nat,n)) 10"
  val input81 =
    "" +
      "(TLambda n:Nat.Lambda x:Vec(Nat,succ succ n).cons[Nat,succ succ succ n](21,cons[Nat,succ succ n](10,x))) 10"
  val input82 =
    "letrec append = TLambda n1:Nat.TLambda n2:Nat. 10 in append"
  val input83 =
    "  letrec plus:Nat->(Nat->Nat) = Lambda x:Nat.Lambda y:Nat.if (iszero y) then x else plus (succ x) (pred y) in plus 100000 1230000"
  val input84 =
    "letrec append:(Pi n:Nat.Vec(Nat,n)->Vec(Nat,succ n)) = Lambda x:Vec("
  val input85 =
    "letrec a:Pi n:*.n->Nat = TLambda n:*.Lambda x:n.10 in (a Unit)"
  val input86 = "letrec append:(Pi n:Nat.Vec(Nat,n)->Vec(Nat,succ n)) = TLambda n:Nat.Lambda x:Vec(Nat,n).cons[Nat,n](10,x) in append 10"
  val input87 =
    "letrec append:(Pi n:Nat.Pi m:Nat.Vec(Nat,n)->(Vec(Nat,m)->Vec(Nat,succ n))) = " +
      "TLambda n:Nat.TLambda m:Nat.Lambda x:Vec(Nat,n).Lambda y:Vec(Nat,m).cons[Nat,n](10,x) in append 10"
  val input88 =
    "head[Nat](cons[Nat,1](27,cons[Nat,0](10,nil[Nat])))"
  val input89 =
    "head[Nat,0](nil[Nat])"
  val input90 =
    "head[Nat,0](cons[Nat,0](10,nil[Nat]))"
  val input91 =
    "head[Nat,1](cons[Nat,0](10,nil[Nat]))"
  val input92 =
    "TLambda n:Nat.Lambda x:Vec(Nat,succ n).head[Nat,n](x)"
  val input93 =
    "("+input92+") "+10
  val input94 =
    "letrec plus:Nat->(Nat->Nat) = Lambda x:Nat.Lambda y:Nat.if (iszero y) then x else plus (succ x) (pred y) in " +
      "letrec append1:Pi n:Nat.Pi m:Nat.Vec(Nat,n)->Vec(Nat,plus n m) = " +
      "TLambda n:Nat.TLambda m:Nat.Lambda x:Vec(Nat,n).if (iszero m) then x else append1 (succ n) (pred m) cons[Nat,n](10,x)" +
      "in append1 0 10 nil[Nat]"
  val input95 =
    "(TLambda n:Nat.Lambda x:Vec(Nat,n).cons[Nat,n](10,x)) 0 nil[Nat]"
  val input96 =
    "cons[Nat,0](10,nil[Nat])"
  val input97 =
    "let tru = TLambda a:*.Lambda x:a.Lambda y:a.x in " +
      "let fls = TLambda a:*.Lambda x:a.Lambda y:a.y in " +
      "let i = TLambda a:*.Lambda x:a.x in " +
      "let is0 = Lambda x:Nat.if (iszero x) then tru else fls in " +
      "is0 0"
  val input98 =
    "Lambda x:Nat.x"
  val input99 =
    "Lambda l:Nat.NatElim(Lambda m:Nat.(Nat->Nat),Lambda n:Nat.n,TLambda k:Nat.Lambda rec:Nat->Nat.Lambda n:Nat.succ (rec n),l)"
  val input100 =
    "("+input99+") 10"
  val input101 =
    "("+input100+") 7"
  val input102 =
    "VecElim(" +
      "Nat," +
      "TLambda m:Nat.TLambda v:Vec(Nat,m).Pi n:Nat.Vec(Nat,plus 0 n)->Vec(Nat,plus m n)," +
      "TLambda n:Nat.Lambda v:Vec(Nat,plus 0 n).v," +
      "TLambda m:Nat." +
      "TLambda v:Nat." +
      "TLambda vs:Vec(Nat,m)." +
      "Lambda rec:(Pi n:Nat.Vec(Nat,plus 0 n)->Vec(Nat,plus m n)).TLambda n:Nat.Lambda w:Vec(Nat,plus 0 n).cons[Nat, plus m n](v,rec n w)," +
      "4," +
      "|10,20,30,40|[Nat])"
  val input103 =
    "|10,20,30,40|[Nat]"
  val input104 =
    "("+input102+") 2 (|87,11|[Nat])"
  val input105 =
    "let plus = "+input99+" in "+input104
  val input106 =
    "((TLambda plus:Nat->(Nat->Nat).("+input102+")) "+input99+") " + "2 (|87,11|[Nat])"
  val input107 =
    "(TLambda plus:Nat->(Nat->Nat).("+input102+")) ("+input99+") " + "2 (|87,11|[Nat])"
  val input110 =
    "(TLambda plus:Nat->(Nat->Nat).cons[Nat,plus 0 0](1,nil[Nat])) "+"("+input99+")"
  val input111 =
    "(Lambda x:Vec(Nat,1).TLambda plus:Nat->(Nat->Nat).cons[Nat,plus 0 1](1,x)) (cons[Nat,0](2,nil[Nat])) ("+input99+")"
  val input112 =
    "Lambda x:Vec(Nat,1).let plus = "+input99+" in cons[Nat,plus 0 1](1,x)) (cons[Nat,0](2,nil[Nat])"
  val input113 =
    "let plus = "+input99+" in Lambda x:Vec(Nat,1). cons[Nat,plus 0 1](1,x))"
  val input114 =
    "let plus = "+input99+" in ("+input102+") 2 (|87,11|[Nat])"
  val input115 =
    "let plus = "+input100+" in "+input102
  val input116 =
    input102
  val input117 =
    "("+input99+")"
  val input118 =
    "(TLambda m:Nat.TLambda v:Vec(Nat,m).Pi n:Nat.Vec(Nat,m)->Vec(Nat,v m n))"
  val input119 =
    "(TLambda k:Nat.Lambda xs:Vec(Nat,k)." +
      "VecElim(" +
      "Nat," +
      "TLambda m:Nat.TLambda v:Vec(Nat,m).Pi n:Nat.Vec(Nat,plus 0 n)->Vec(Nat,plus m n)," +
      "TLambda n:Nat.Lambda v:Vec(Nat,plus 0 n).v," +
      "TLambda m:Nat." +
      "TLambda v:Nat." +
      "TLambda vs:Vec(Nat,m)." +
      "Lambda rec:(Pi n:Nat.Vec(Nat,plus 0 n)->Vec(Nat,plus m n)).TLambda n:Nat.Lambda w:Vec(Nat,plus 0 n).cons[Nat, plus m n](v,rec n w)," +
      "k," +
      "xs))"
  val input120 =
    input119+" 4 (|10,20,30,40|[Nat]) 2 (|20,45|[Nat])"
  val input121 =
    "("+input116+") 2 (|67,54|[Nat])"
  val input122 =
    "succ succ succ 10"
  val input123 =
    "(Lambda n:Nat.succ n) ((Lambda n:Nat.succ ((Lambda n:Nat.succ n) n)) 2)"
  val input124 =
    "(tail[Nat,plus k n] (("+input119+") k nil[Nat] n nil[Nat])))"
  val input125 =
    "TLambda n:Nat.TLambda T:*.TLambda v:Vec(T,n).VecElim(T,TLambda m:Nat.TLambda v1:Vec(T,m).Bool,true,TLambda l:Nat.TLambda h:T.TLambda t:Vec(T,l).Lambda rec:Bool. false,n,v)"
  val input126 =
    "TLambda n:Nat.TLambda v:Vec(*,n).TLambda T:*."+
    "VecElim(" +
      "*," +
      "TLambda m:Nat.TLambda v1:Vec(*,m).*," +
      "T,"+
      "TLambda m:Nat.  "+
        "TLambda h:*.    "+
          " TLambda t:Vec(*,m). "+
            "Lambda rec:*.       "+
              " h->rec,n,v" +
    ")"
  val input127 = "x"
  val input128 = "Lambda x:Nat.x"
  val input129 = "Lambda x:Nat.Lambda y:Nat.y"
  val input130 = "Lambda x:Nat->Nat.Lambda y:Nat.x y"
  val input131 = "(Lambda x:Nat.x) 10"
}
