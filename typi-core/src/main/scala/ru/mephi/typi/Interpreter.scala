package ru.mephi.typi

import core.exception.TypeMismatchException
import core.Index
import core.term.DerivationTree
import core.VarBind
import core.{VarBind, Index, Binding, Name}
import core.term._
import util.parsing.combinator.JavaTokenParsers
import ru.mephi.typi.SuccessEval
import ru.mephi.typi.TypeError
import ru.mephi.typi.Assign

object Interpreter extends JavaTokenParsers{
  val parser = new LangParser

  def main(args:Array[String]){
    //println(this.eval("Lambda x:Nat.x"))
    //val input1 =
    //  "Lambda y:Nat.Lambda x:Nat.x"
    //val  input2 =
    //  "(TLambda Tx:*.TLambda Ty:*.Lambda x:Tx.Lambda y:Ty.TLambda R:*.Lambda r:Tx->(Ty->R).r x y)"
    //println(Printer(Typer.typeof1(LangParser.convertToDB1(parser.parse(parser.Commands, parser.parse(parser.Lex,
    //  input2
    //).get).get.head.term, Nil), Nil, Nil, 0)))
    var env:List[Term] = Nil
    var ctx:List[(Name,Binding)] =Nil
    var IndexList:List[String] = Nil

    for (filename<- args){
      try {
        val source = scala.io.Source.fromFile(filename).mkString

        println("Файл: "+filename)


		//TODO: create method from this code (in Interpreter)
        for (result<-this.eval(source)) {
          println(result.result)
          /*result match {
            case SuccessEval(evalLog, _) =>
              for (step<-evalLog.reverse)
                println("::" + step)
            case _ => ()
          }*/
        }
      }
      catch {
        case e:java.io.FileNotFoundException => println("Файл "+filename+" не найден!")
        //case e:java.lang.RuntimeException => println("Синтаксическая ошибка в файле "+filename)
        //case e:java.lang.RuntimeException => println("Ошибка синтаксиса!")
        //case e:TypeMismatchException => println("Несоответсвие типов! "+e.msg)
      }

    }
  }

  def eval(source:String):Iterable[EvalResult] = {
    var env:List[Term] = Nil
    var ctx:List[(Name,Binding)] =Nil
    var IndexList:List[String] = Nil

    for (comand <- commands(source)) yield {
      try {
        val tree = LangParser.convertToDB1(comand.term, IndexList) //parser.MakeTree(comand)
        println("Tree = "+tree)
        val derivationTree = Typer.typeof1(tree, ctx, env, 0)
        //val ty = derivationTree.resultType//tree.typeof(ctx, env, 0)
        val ty = tree.typeof(ctx, env, 0)
        val result = tree.eval(env,0,List(tree))
        comand match {
          case Assign(name, _) =>
            env = result.head :: env
            ctx = (Index(0, name), VarBind(/*derivationTree.resultType*/ty)) :: ctx
            IndexList = name :: IndexList
          case _ => ()
        }
        //">>" + result + "::" + ty
        SuccessEval(result, DerivationTree("",Nil, Corollary(ctx, tree, ty))/*derivationTree*/)
      }
      catch {
        //case e:java.lang.RuntimeException => println("Синтаксическая ошибка в файле "+filename)
        //case e:java.lang.RuntimeException => println("Ошибка синтаксиса!")
        case e: TypeMismatchException => TypeError("Несоответсвие типов! " + e.msg + " " + "env = "+env + " ctx = " +ctx )
      }
    }
  }

  def commands(sourse:String):List[Command] = {
    val r = parser.parse(parser.Commands, parser.parse(parser.Lex,sourse).get).get
    //println(r)
    r
  }
}

trait EvalResult {
  def result:String
}

case class SuccessEval(evalLog:List[Term], derivationTree:DerivationTree) extends EvalResult{
  override def result:String = ">>"+(evalLog.head)+":"+derivationTree.resultType
  //def term = evalLog.head
  //def termType = ty
}

case class TypeError(msg:String) extends EvalResult {
  override def result:String = msg
}
