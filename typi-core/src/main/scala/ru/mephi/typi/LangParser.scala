package ru.mephi.typi

import scala.util.parsing.combinator._
import ru.mephi.typi.core.term._
import ru.mephi.typi.core._
import Input._

class Command(val term:Term)
case class Assign(name:String,override val  term:Term) extends Command(term)


class LangParser extends JavaTokenParsers {

  def Term:Parser[Term] = (
     ProjTerm
   | LambdaTerm
   | TLambdaTerm
   | UnpackPairTerm
   | LetRecTerm
   | LetTerm
   | IfTerm
   | NatElimTerm
   | VecElimTerm
   | AppTerm
  )

  def UnpackPairTerm:Parser[Term] = (
    "#let"~"{"~"##"~"""[a-zA-Z_]\w*""".r~","~"##"~"""[a-zA-Z_]\w*""".r~"}"~"="~Term~"#in"~Term ^^ {case  "#let"~"{"~"##"~n1~","~"##"~n2~"}"~"="~t1~"#in"~t2 => TmUnpackExistPair((n1,n2),t1,t2)}
  )

  def NatElimTerm[Term] = (
    "#NatElim"~"("~Term~","~Term~","~Term~","~Term~")" ^^ {case "#NatElim"~"("~m~","~mz~","~ms~","~l~")" => TmNatElim(m,mz,ms,l)}
  )

  def VecElimTerm[Term] = (
    "#VecElim"~"("~Term~","~Term~","~Term~","~Term~","~Term~","~Term~")" ^^ {case "#VecElim"~"("~ty~","~m~","~mz~","~ms~","~k~","~l~")" => TmVecElim(ty,m,mz,ms,k,l)}
    )

  def ProjTerm:Parser[TmProj] = (
     ATerm~".##_"~"""[a-zA-Z_]\w*""".r ^^ {case t~".##_"~proj => TmProj(t,proj)}
  )

  def LambdaTerm:Parser[Term] = (
    "#Lambda"~"##"~"""[a-zA-Z_]\w*""".r~":"~/*TypeTerm*/Term~"."~Term ^^ {case "#Lambda"~"##"~v1~":"~ty~"."~t1 => Lambda(t1,ty,v1)}
  )

  def TLambdaTerm:Parser[Term] = (
    "#TLambda"~"##"~"""[a-zA-Z_]\w*""".r~":"~TypeTerm~"."~Term ^^ {case "#TLambda"~"##"~v1~":"~ty~"."~t1 => TLambda(t1,ty,v1)}
  )

  def LetTerm:Parser[Term] = (
    "#let"~"##"~"""[a-zA-Z_]\w*""".r~"="~Term~"#in"~Term ^^ {case "#let"~"##"~v~"="~t1~"#in"~t2 => Let(v, t1, t2)}
  )
  def LetRecTerm:Parser[Term] = {
    "#letrec"~"##"~"""[a-zA-Z_]\w*""".r~":"~TypeTerm~"="~Term~"#in"~Term ^^ {case "#letrec"~"##"~v~":"~ty~"="~t1~"#in"~t2 => Let(v,Fix(Lambda(t1,ty,v)),t2)}
  }
  def IfTerm:Parser[Term] = (
    "#if"~Term~"#then"~Term~"#else"~Term ^^ {case "#if"~t1~"#then"~t2~"#else"~t3 => TmIf(t1,t2,t3)}
  )
  def ConstTerm:Parser[Term] = (
    ("#true" | "#false") ^^ (s => GetBool(s))
    |"#unit" ^^ (s => TmUnit)
    | "~" ^^ (s => UnknownType(0,""))
  )

  def AppTerm:Parser[Term] = (
    ATerm~rep(ATerm) ^^ {case t~tl =>(t/:tl)((t1:Term, t2:Term)=>App(t1,t2))}
      |   ATerm
    )

  def TypeTerm:Parser[Term] = (
      RecordType
    | ArrType
    | PiType
    | SiType
    | IfTerm
  )

  def ArrType:Parser[Term] = (
    AType~"->"~AType ^^ {case ty1~"->"~ty2 => TyArr(ty1,ty2)}
  | AType
  )

  def PiType:Parser[Type] = (
    "#Pi"~"##"~"""[a-zA-Z_]\w*""".r~":"~AType~"."~TypeTerm ^^ {case "#Pi"~"##"~v~":"~ty1~"."~ty2 => TyPi(v,ty1,ty2)}
  )

  def SiType:Parser[Type] = (
    "#Si"~"##"~"""[a-zA-Z_]\w*""".r~":"~AType~"."~TypeTerm ^^ {case "#Si"~"##"~v~":"~ty1~"."~ty2 => TySigma(v,ty1,ty2)}
  )

  def AType:Parser[Term] = (
    "("~TypeTerm~")" ^^ {case "("~ty~")" => ty}
  | "#Bool" ^^ {s => TyBool}
  | "#Unit" ^^ {s => TyUnit}
  | "#Nat" ^^ {s => TyNat}
  | "*" ^^ {s => Star}
  | "#Vec"~"("~TypeTerm~","~Term~")" ^^ {case "#Vec"~"("~ty~","~t~")" => TyVec(ty,t)}
  | VarTerm
  )

  def ATerm:Parser[Term] = (
    "("~Term~")" ^^ {case "("~t~")" => t}
  | ExistTerm
  | RecordTerm
  | ListTerm
  | ConstTerm
  | NatTerm
  | VecTerm
  | TypeTerm
  //| VarTerm
  )

  def ExistTerm:Parser[Term] = (
    "{"~Term~","~Term ~"}"~"#as"~SiType ^^ {case "{"~t1~","~t2 ~"}"~"#as"~ann => TmExistPair(t1, t2, ann)}
  )

  def VecTerm:Parser[Term] = {(
       "#nil"~"["~Term~"]" ^^ {case "#nil"~"["~t~"]"=> TmNil(t)}
      |  "#cons"~"["~Term~","~Term~"]"~"("~Term~","~Term~")"    ^^ {case "#cons"~"["~ty~","~n~"]"~"("~head~","~tail~")" => TmCons(ty,n,head,tail)}
      |  "#head"~"["~Term~","~Term~"]"~"("~Term~")" ^^ {case "#head"~"["~ty~","~n~"]"~"("~list~")" => TmHead(ty,n,list)}
      |  "#tail"~"["~Term~","~Term~"]"~"("~Term~")" ^^ {case "#tail"~"["~ty~","~n~"]"~"("~list~")" => TmTail(ty,n,list)}
  )}

  def NatTerm:Parser[Term] = (
      "#succ" ~ Term ^^ {case "#succ"~t => TmSucc(t)}
    | "#pred" ~ Term ^^ {case "#pred"~t => TmPred(t)}
    | "#iszero" ~ Term ^^ {case "#iszero"~t => TmIsZero(t)}
    | "##"~"""[1234567890]\w*""".r ^^ {case "##"~n => TmNat(n.toInt)}
  )

  def VarTerm:Parser[Term] = (
    "##"~"""[a-zA-Z_]\w*""".r ^^ {case "##"~a => Var(Literal(a))}
  )

  def Lex:Parser[String] = (
	  """/\*(.|[\r\n])*?\*/""".r ~ Lex ^^ {case c~d => d}
  |  ("@" | "(" | ")" | "."|","| "="|":"|"->"|"{"|"}"|"._"|"*"|"["|"]"|"|"|"|"|";"|"~") ~ Lex ^^ {case l1~l2 => l1 + l2}
  |  """[1234567890]\w*""".r ~ Lex ^^ {case n~l => "##"+n+" "+l}
  |  """[a-zA-Z_]\w*""".r ~ Lex ^^ {
       case (kw@("Si"|"as"|"assign"|"VecElim"|"NatElim"|"head"|"tail"|"Vec"|"cons"|"nil"|"Pi"|"TLambda"|"Lambda" |"let"|"in"|"if"|"then"|"else"|"true"|"false"|"Bool"|"Unit"|"unit"|"letrec"|"succ"|"iszero"|"pred"|"Nat"))~rest => "#"+kw+" "+rest
       case id~rest => "##"+id+rest
  }
  |  "" ^^ {s=>""}
  )

  abstract class MyTerm
  case class MyApp(t1:String,t2:String) extends MyTerm
  case class Id(id:String) extends MyTerm


  def GetBool(str:String) = str match {
    case "#true" => TmTrue
    case "#false" => TmFalse
  }

  def MakeTree(str:String,IndexList:List[String]) = {
    LangParser.convertToDB1((this.parse(this.Term, this.parse(this.Lex,str).get).get),IndexList)
  }

  def MakeTree(str:String):Term = {
    LangParser.convertToDB((this.parse(this.Term, this.parse(this.Lex,str).get).get))
  }


  //Область экспериментов

  def RecordTerm:Parser[TmRecord] = (
    "{"~Field~rep(","~Field)~"}" ^^ {case "{"~s1~s2~"}" => TmRecord(((s1)::(for (x<-s2) yield (x._2))).toMap)}
  )

  def ListTerm:Parser[Term] = (
    "|"~Term~rep(","~Term)~"|"~"["~Term~"]" ^^ {case "|"~h~t~"|"~"["~ty~"]" => createVec(TmNil(ty),(h::t.map(s=>s._2)).reverse,0,ty)}
  )

  private def createVec(r:Term,t:List[Term],l:Int,ty:Term):Term = t match {
    case Nil => r
    case x::xs => createVec(TmCons(ty,TmNat(l),x,r),xs,l+1,ty)
  }

  def Field:Parser[(String,Term)] = (
    "##"~"""[a-zA-Z_]\w*""".r~"="~ Term ^^ {case "##"~s1~"="~s2 => (s1.toString,s2)}
  )

  def RecordType:Parser[TyRecord] = (
    "{"~RecordTypeField~rep(","~RecordTypeField)~"}" ^^ {case "{"~s1~s2~"}" => TyRecord(((s1)::(for (x<-s2) yield (x._2))).toMap)}
    )

  def RecordTypeField:Parser[(String,Term)] = (
    "##"~"""[a-zA-Z_]\w*""".r~":"~ TypeTerm ^^ {case "##"~s1~":"~s2 => (s1.toString,s2)}
  )

  def Proj:Parser[(String,String)] = (
    "##"~"""[a-zA-Z_]\w*""".r~"._"~"""[a-zA-Z_]\w*""".r ^^ {case "##"~t~"._"~t2 => (t,t2)}
  )


  def Commands:Parser[List[Command]] = (
    this.AtomCommand~rep(";"~this.AtomCommand) ^^ {case c1~cs => c1::(for (c<-cs) yield (c._2))}
  )

  def AtomCommand:Parser[Command] = (
    "#assign"~"##"~"""[a-zA-Z_]\w*""".r~"="~Term ^^ {case "#assign"~"##"~name~"="~t => Assign(name,t)}
      | Term ^^ {t => new Command(t)}
  )
}


object LangParser extends JavaTokenParsers{
  def convertToDB(t1:Term):Term = convertToDB1(t1,Nil)

  def convertToDB1(t:Term, IndexList:List[String]):Term = t match {
    case App(t1,t2) => App(convertToDB1(t1,IndexList),convertToDB1(t2,IndexList))
    case Lambda(t1,ty,name) => Lambda(convertToDB1(t1,name::IndexList),convertToDB1(ty,IndexList),name)
    case TLambda(t1,ty,name) => TLambda(convertToDB1(t1,name::IndexList),convertToDB1(ty,IndexList),name)
    case Var(name) => name match {
      case Literal(name1) => Var(getName(name1, IndexList, 0))
      case x@Index(_,_) => Var(x)
    }
    case Let(name, t1, t2) => Let(name, convertToDB1(t1,IndexList), convertToDB1(t2, name::IndexList))
    case TmIf(t1,t2,t3) => TmIf(convertToDB1(t1,IndexList),convertToDB1(t2,IndexList),convertToDB1(t3,IndexList))
    case TmRecord(fields) => TmRecord(fields.mapValues(t => convertToDB1(t,IndexList)))
    case TyRecord(fields) => TyRecord(fields.mapValues(t => convertToDB1(t,IndexList)))
    case TmProj(t1,p) => TmProj(convertToDB1(t1,IndexList),p)
    case Fix(t1) => Fix(convertToDB1(t1,IndexList))
    case TmSucc(t1) => TmSucc(convertToDB1(t1,IndexList))
    case TmPred(t1) => TmPred(convertToDB1(t1,IndexList))
    case TmIsZero(t1) => TmIsZero(convertToDB1(t1,IndexList))
    case TyArr(ty1,ty2) => TyArr(convertToDB1(ty1,IndexList),convertToDB1(ty2,"-"::IndexList))
    case TyPi(name,ty1,ty2) => TyPi(name, convertToDB1(ty1,IndexList), convertToDB1(ty2,name::IndexList))
    case TyVec(ty,n) => TyVec(convertToDB1(ty,IndexList),convertToDB1(n,IndexList))
    case TmNil(ty) => TmNil(convertToDB1(ty,IndexList))
    case TmCons(ty,n,head,tail) => TmCons(convertToDB1(ty,IndexList),convertToDB1(n,IndexList),convertToDB1(head,IndexList),convertToDB1(tail,IndexList))
    case TmHead(ty,n,list) => TmHead(convertToDB1(ty,IndexList),convertToDB1(n,IndexList),convertToDB1(list,IndexList))
    case TmTail(ty,n,list) => TmTail(convertToDB1(ty,IndexList),convertToDB1(n,IndexList),convertToDB1(list,IndexList))
    case TmNatElim(m,mz,ms,l) => TmNatElim(convertToDB1(m,IndexList),convertToDB1(mz,IndexList),convertToDB1(ms,IndexList),convertToDB1(l,IndexList))
    case TmVecElim(ty,m,mz,ms,k,l) => TmVecElim(convertToDB1(ty,IndexList),convertToDB1(m,IndexList),convertToDB1(mz,IndexList),convertToDB1(ms,IndexList),convertToDB1(k,IndexList),convertToDB1(l,IndexList))
    case TySigma(name,ty1,ty2) => TySigma(name, convertToDB1(ty1,IndexList), convertToDB1(ty2,name::IndexList))
    case TmExistPair(t1,t2,ann) => TmExistPair(convertToDB1(t1, IndexList),convertToDB1(t2, IndexList),convertToDB1(ann, IndexList))
    case TmUnpackExistPair(pattern, t1, t2) => TmUnpackExistPair(pattern, convertToDB1(t1, IndexList), convertToDB1(t2, pattern._1::pattern._2::IndexList))
    case _ => t
  }

  private def getName(name:String, IndexList:List[String], index:Int):Name = IndexList match {
    case x::xs if x == name => Index(index, name)
    case x::xs => getName(name, xs, index+1)
    case Nil => Literal(name)
  }

  def main(args:Array[String]){

    val x = new LangParser()
    val input = input126

    println(x.parse(x.Term,x.parse(x.Lex,input).get))
    println("input : "+x.MakeTree(input,List("n","k","plus")))
    //println("result: "+x.MakeTree(input).eval)
    //println("typeTerm  : "+x.MakeTree(input).typeof)
    //println("result: "+x.MakeTree(input124,List("plus")).eval(List(x.MakeTree(input99))))
    println("typeTerm  : "+x.MakeTree(input,List("n","k","plus")).typeof(
      List(
        (Index(0,"n")   ,VarBind(TyNat)),
        (Index(0,"k")   ,VarBind(TyNat)),
        (Index(0,"plus"),VarBind(TyArr(TyNat,TyArr(TyNat,TyNat))))),
      List(
        TmNat(0),
        TmNat(0),
        x.MakeTree(input99)),
      0)
    )
  }
}
