package ru.mephi.typi.SimpleLambda

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:34
* To change this template use File | Settings | File Templates.
*/
trait Const extends Term {
  def getType:Type = this match {
    case TmTrue | TmFalse => TyBool
    case TmUnit => TyUnit
  }
}
