package ru.mephi.typi.SimpleLambda

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:36
* To change this template use File | Settings | File Templates.
*/
case class Index(i:Int, name:String) extends Name {
  override def equals(other: Any) = other match {
    case Index(i2, _) =>  i == i2
    case _ => false
  }

  override def hashCode = 41 * (41 + i.hashCode())
}
