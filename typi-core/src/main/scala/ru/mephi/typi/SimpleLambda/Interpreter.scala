package ru.mephi.typi.SimpleLambda

import util.parsing.combinator.JavaTokenParsers
import ru.mephi.typi.LangParser
import ru.mephi.typi.core.exception.TypeMismatchException

object Interpreter extends JavaTokenParsers {

  val parser = new LangParser

  def main(args:Array[String]){

    //val parser = new LangParser

    for (filename<- args){
      try {
        val source = scala.io.Source.fromFile(filename).mkString

        println("Файл: "+filename)

		//TODO: create method from this code (in Interpreter)
        for (comand <- comands(source)){
          val tree = parser.MakeTree(comand)
          //println(tree)
          val ty = tree.typeof(Nil)
          val result = tree.eval

          println(">>"+result + "::" + ty)
        }
      }
      catch {
        case e:java.io.FileNotFoundException => println("Файл "+filename+" не найден!")
        //case e:java.lang.RuntimeException => println("Синтаксическая ошибка в файле "+filename)
        //case e:java.lang.RuntimeException => println("Ошибка синтаксиса!")
        case e:TypeMismatchException => println("Несоответсвие типов!")
      }
    }

  }

  /*def eval(source:String):Iterable[String] = {
    try {
      for (comand <- comands(source)){
        val tree = parser.MakeTree(comand)
        //println(tree)
        val ty = tree.typeof(Nil)
        val result = tree.eval

        println(">>"+result + "::" + ty)
      }
    }
    catch {
      case e:java.io.FileNotFoundException => println("Файл "+filename+" не найден!")
      //case e:java.lang.RuntimeException => println("Синтаксическая ошибка в файле "+filename)
      //case e:java.lang.RuntimeException => println("Ошибка синтаксиса!")
      case e:TypeMismatchException => println("Несоответсвие типов!")
    }
  }*/

  private def comands(input:String):Array[String] = input.split(';')
}
