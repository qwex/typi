package ru.mephi.typi.SimpleLambda

import ru.mephi.typi.SimpleLambda

class NoRuleApplies(t:Term) extends Exception {
  def Result = t
}
