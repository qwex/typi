package ru.mephi.typi.SimpleLambda

import ru.mephi.typi.core.exception.TypeMismatchException
import ru.mephi.typi.SimpleLambda.NoRuleApplies

object SimpleLambda {

  def indexShift(shift:Int ,ty:Term,shiftBound:Int):Term = ty match {
    case TyArr(ty1,ty2) => TyArr(indexShift(shift,ty1,shiftBound),indexShift(shift,ty2,shiftBound+1))
    case TyVec(ty1,n1) => TyVec(indexShift(shift,ty1,shiftBound),indexShift(shift,n1,shiftBound))
    case TmSucc(t1) => TmSucc(indexShift(shift,t1,shiftBound))
    case TmPred(t1) => TmPred(indexShift(shift,t1,shiftBound))
    case TyPi(name,ty1,ty2) =>  TyPi(name,indexShift(shift,ty1,shiftBound),indexShift(shift,ty2,shiftBound+1))
    case Lambda(body,ty,name) => Lambda(indexShift(shift,body,shiftBound+1),indexShift(shift,ty,shiftBound),name)
    case TLambda(body,ty,name) => TLambda(indexShift(shift,body,shiftBound+1),indexShift(shift,ty,shiftBound),name)
    case App(t1,t2) => App(indexShift(shift,t1,shiftBound),indexShift(shift,t2,shiftBound))
    case TmCons(ty1,n,head,tail) => TmCons(indexShift(shift,ty1,shiftBound),indexShift(shift,n,shiftBound),indexShift(shift,head,shiftBound),indexShift(shift,tail,shiftBound))
    case TmNatElim(m,mz,ms,l) => TmNatElim(indexShift(shift,m,shiftBound),indexShift(shift,mz,shiftBound),indexShift(shift,ms,shiftBound),indexShift(shift,l,shiftBound))
    case TmVecElim(ty1,m,mz,ms,k,list) =>
      TmVecElim(indexShift(shift,ty1,shiftBound),
        indexShift(shift,m,shiftBound),
        indexShift(shift,mz,shiftBound),
        indexShift(shift,ms,shiftBound),
        indexShift(shift,k,shiftBound),
        indexShift(shift,list,shiftBound))
    case v @ Var(name) => name match {
      case Index(i,n) if i>= shiftBound =>
        Var(Index(i+shift,n))
      case  _ => v
    }
    case  _ => ty
  }

  def substInType(t1:Term, i:Int, t2:Term):Term = t2 match {
    case TyArr(ty1,ty2) => TyArr(substInType(t1,i,ty1),substInType(indexShift(1,t1,0),i+1,ty2))
    case TyPi(name,ty1,ty2) => TyPi(name,substInType(t1,i,ty1),substInType(indexShift(1,t1,0),i+1,ty2))
    case TyRecord(fields) => TyRecord(fields.mapValues(t => substInType(t1,i,t)))
    case Lambda(body,ty,name) =>
      Lambda(substInType(indexShift(1,t1,0),i+1,body),substInType(t1, i, ty), name)
    case Var(x) => x match {
      case Index(j,_) if i == j => t1
      case _ => Var(x)
    }
    case _ => generalSubst(t1,i,t2,substInType)
  }

  def subst(t1:Term, i:Int, t2:Term):Term = t2 match {
    case Lambda(body,ty,name) =>
      Lambda(subst (indexShift(1,t1,0), i+1, body),substInType(t1, i, ty),name)
    case _ => generalSubst(t1,i,t2,subst)
  }

  def generalSubst(t1:Term, i:Int, t2:Term, subst:(Term, Int, Term) => Term):Term = {t2} match {
    case Var(x) => x match {
      case Index(j,_) if i == j => t1
      case _ => Var(x)
    }
    case TLambda(body,ty,name) =>
      TLambda(subst (indexShift(1,t1,0), i+1, body),substInType(t1, i, ty),name)
    case TyArr(ty1,ty2) => TyArr(substInType(t1,i,ty1),substInType(indexShift(1,t1,0),i+1,ty2))
    case TyPi(name,ty1,ty2) => TyPi(name,substInType(t1,i,ty1),substInType(indexShift(1,t1,0),i+1,ty2))
    case App(appT1, appT2) => App(subst (t1, i, appT1),subst(t1, i, appT2))
    case Let(name, t21, t22) => Let(name, subst(t1, i, t21), subst(t1, i+1, t22))
    case TmIf(t21,t22,t23) => TmIf(subst(t1,i,t21), subst(t1,i,t22), subst(t1,i,t23))
    case TmRecord(fields) => TmRecord(fields.mapValues(t => subst(t1,i,t)))
    case TmProj(t,p) => TmProj(subst(t1,i,t),p)
    case Fix(t) => Fix(subst(t1,i,t))
    case TmIsZero(t11) => TmIsZero(subst(t1,i,t11))
    case TmSucc(t11) => TmSucc(subst(t1,i,t11))
    case TmPred(t11) => TmPred(subst(t1,i,t11))
    case TmNil(ty) => TmNil(substInType(t1,i,ty))
    case TmCons(ty,n,head,tail) => TmCons(substInType(t1,i,ty),subst(t1,i,n),subst(t1,i,head),subst(t1,i,tail))
    case TyVec(ty,n) => TyVec(substInType(t1,i,ty),subst(t1,i,n))
    case TmHead(ty,n,list) => TmHead(substInType(t1,i,ty),subst(t1,i,n),subst(t1,i,list))
    case TmTail(ty,n,list) => TmTail(substInType(t1,i,ty),subst(t1,i,n),subst(t1,i,list))
    case TmNatElim(m,mz,ms,l) => TmNatElim(subst(t1,i,m),subst(t1,i,mz),subst(t1,i,ms),subst(t1,i,l))
    case TmVecElim(ty,m,mz,ms,k,list) => TmVecElim(subst(t1,i,ty),subst(t1,i,m),subst(t1,i,mz),subst(t1,i,ms),subst(t1,i,k),subst(t1,i,list))
    //TODO: разобраться с дубликатом!
    case TyPi(name,ty1,ty2) => TyPi(name,substInType(t1,i,ty1),substInType(t1,i+1,ty2))
    case _ => t2
  }

  def isval(t:Term,boundLevel:Int):Boolean = t match {
    case Lambda(_,_,_) => true
    case Var(Literal(_)) => true
    case Var(Index(i,name)) if i<boundLevel => true

    //TODO:убрать
    case App(Lambda(_,_,_),t1) => false
    case App(v1 @ Var(_),v2) if isval(v1,boundLevel) && isval(v2,boundLevel) => true
    case App(t1,t2) => isval(t1,boundLevel) && isval(t2,boundLevel)

    case TmTrue | TmFalse | TmUnit => true
    case TmRecord(fields) => for ((_,t)<-fields if !isval(t,boundLevel)) return false; true
    case TyNat|TyBool|TyUnit|TyArr(_,_)|TyPi(_,_,_)|TyRecord(_) | TyPi(_,_,_) => true
    case TmNil(_) => true // в будущем, когда типы будут вычисляться, может и не надо убирать
    case TmCons(ty,n,head,tail) => isval(n,boundLevel) && isval(head,boundLevel) && isval(tail,boundLevel)
    case TmSucc(t1) => isval(t1,boundLevel) //false убрать в будущем
    case TmNatElim(_,_,_,TmSucc(l)) => false
    case TmNatElim(m,mz,ms,l) => !isnumeral(l) && isval(m,boundLevel) && isval(mz,boundLevel) && isval(ms,boundLevel) && isval(l,boundLevel)
    //case TmVecElim(_,m,mz,ms,k,list) => isval(m) && isval(mz) && isval(ms) && isval(k) && isval(list)
    case _ => isnumeral(t) //|| false
  }

  def isnumeral(t:Term):Boolean = t match {
    //case TmSucc(t1) => isnumeral(t1)
    case TmNat(_) => true
    case TmSucc(n) => isnumeral(n)
    case _ => false
  }

  def eval1(t:Term,env:List[Term],boundLevel:Int):Term = {t match {
    case TLambda(t1,ty,name) => Lambda(t1,ty,name)
    case App(TLambda(body,ty,name), t2) => App(Lambda(body, ty, name),t2)
    case App(Lambda(t1,_,_), v2) if isval(v2,boundLevel) =>
      indexShift(-1,subst(indexShift(1,v2,0), 0, t1),0)
    case App(v1,v2) if isval(v1,boundLevel) && isval(v2,boundLevel) => throw new NoRuleApplies(t)
    case App(v1,t2) if isval(v1,boundLevel) =>
      App(v1,eval1(t2,env,boundLevel))
    //case App(Var(_),Var(_)) =>
    case App(t1,t2) =>
      App(eval1(t1,env,boundLevel),t2)
    case Let(_, v1, t2) if (isval(v1,boundLevel)) =>
      subst(v1, 0, t2)
    case Let(name, t1, t2) =>
      Let(name, eval1(t1,env,boundLevel), t2)
    case f @ Fix(v) if isval(v,boundLevel) =>
      v match {
        case Lambda(body, _, _) =>
          subst(f,0,body)
        case _ => throw new NoRuleApplies(t)
      }
    case Fix(t1) =>
      Fix(eval1(t1,env,boundLevel))
    case TmIf(v,t1,t2) if isval(v,boundLevel) => v match {
      case TmTrue => t1
      case TmFalse => t2
    }
    case TmIf(t1,t2,t3) => {
      TmIf(eval1(t1,env,boundLevel),t2,t3) }

    case TmRecord(fields) => {
      def evalrecord(fields:List[(String,Term)]):List[(String,Term)] = fields match {
        case Nil => throw new NoRuleApplies(t)
        case (l,v)::xs if isval(v,boundLevel) => (l,v)::evalrecord(xs)
        case (l,t1)::xs => ((l,eval1(t1,env,boundLevel))::xs)
      }
      TmRecord(evalrecord(fields.toList).toMap)
    }

    case TmProj(v@TmRecord(fields),p) if isval(v,boundLevel) =>
      fields(p)
    case TmProj(t1,p) =>
      TmProj(eval1(t1,env,boundLevel),p)

    case TmSucc(TmPred(t1)) => t1
    case TmSucc(v) if isnumeral(v) => v match {
      case TmNat(i) => TmNat(i+1)
      case ts @ TmSucc(t1) => TmSucc(eval1(ts,env,boundLevel))
    }
    case TmSucc(v) if isval(v,boundLevel) => v match {
      case _ => throw new NoRuleApplies(t)
    }

    case TmSucc(t1) =>
      eval(TmSucc(eval(t1,env,boundLevel)),env,boundLevel)

    case TmIsZero(t:Term) => t match {
      case TmNat(n) => if (n == 0) TmTrue else TmFalse
      case TmSucc(t1) if isnumeral(t1) => TmFalse
      case _ => TmIsZero(eval1(t,env,boundLevel))
    }

    case TmPred(t1) => t1 match{
      case TmNat(i) => if (i==0) TmNat(0) else TmNat(i-1)
      case TmSucc(t2) if isnumeral(t2) => t2
      case _ => throw new NoRuleApplies((TmPred(eval(t1,env,boundLevel))))
    }


    case TmCons(_,_,_,_) if isval(t,boundLevel) => throw new NoRuleApplies(t)
    case TmCons(ty,vn,vhead,tail) if (isval(vn,boundLevel) && isval(vhead,boundLevel)) =>
      TmCons(ty,vn,vhead,eval1(tail,env,boundLevel))
    case TmCons(ty,vn,head,tail) if isval(vn,boundLevel) =>
      TmCons(ty,vn,eval1(head,env,boundLevel),tail)
    case TmCons(ty,n,head,tail) =>
      TmCons(ty,eval1(n,env,boundLevel),head,tail)

    case TmHead(_,_,t1) => t1 match {
      case TmCons(ty,n,head,tail) => head
    }

    case TmTail(_,_,t1) => t1 match {
      case TmCons(ty,n,head,tail) => tail
    }

    case TmNatElim(vm,vmz,vms,vl) if List(vm,vmz,vms,vl).map(v=>isval(v,boundLevel)).fold(true)(_ && _) =>
      vl match {
        case TmNat(n) if n == 0 => vmz
        case TmNat(n) => App(App(vms,TmNat(n-1)),TmNatElim(vm,vmz,vms,TmNat(n-1)))
        case TmSucc(l) => App(App(vms,l),TmNatElim(vm,vmz,vms,l))
        case _ => throw new NoRuleApplies(t)
      }
    case TmNatElim(vm,vmz,vms,l) if List(vm,vmz,vms).map(v=>isval(v,boundLevel)).fold(true)(_ && _) =>
      TmNatElim(vm,vmz,vms,eval1(l,env,boundLevel))
    case TmNatElim(vm,vmz,ms,l) if List(vm,vmz).map(v=>isval(v,boundLevel)).fold(true)(_ && _) =>
      TmNatElim(vm,vmz,eval1(ms,env,boundLevel),l)
    case TmNatElim(vm,mz,ms,l) if isval(vm,boundLevel) =>
      TmNatElim(vm,eval1(mz,env,boundLevel),ms,l)
    case TmNatElim(m,mz,ms,l) =>
      TmNatElim(eval1(m,env,boundLevel),mz,ms,l)

    case TmVecElim(ty,vm,vmz,vms,vk,vl) if List(vm,vmz,vms,vk,vl).map(v=>isval(v,boundLevel)).fold(true)(_ && _) =>
      vl match {
        case TmNil(_) => vmz
        case TmCons(_,n,head,tail) => App(App(App(App(vms,n),head),tail),TmVecElim(ty,vm,vmz,vms,n,tail))
        case _ => throw new NoRuleApplies(t)
      }

    case TmVecElim(ty,vm,vmz,vms,vk,l) if List(vm,vmz,vms,vk).map(v=>isval(v,boundLevel)).fold(true)(_ && _) =>
      TmVecElim(ty,vm,vmz,vms,vk,eval1(l,env,boundLevel))
    case TmVecElim(ty,vm,vmz,vms,k,l) if List(vm,vmz,vms).map(v=>isval(v,boundLevel)).fold(true)(_ && _) =>
      TmVecElim(ty,vm,vmz,vms,eval1(k,env,boundLevel),l)
    case TmVecElim(ty,vm,vmz,ms,k,l) if List(vm,vmz).map(v=>isval(v,boundLevel)).fold(true)(_ && _) =>
      TmVecElim(ty,vm,vmz,eval1(ms,env,boundLevel),k,l)
    case TmVecElim(ty,vm,mz,ms,k,l) if isval(vm,boundLevel) =>
      TmVecElim(ty,vm,eval1(mz,env,boundLevel),ms,k,l)
    case TmVecElim(ty,m,mz,ms,k,l) =>
      TmVecElim(ty,eval1(m,env,boundLevel),mz,ms,k,l)

    case Var(Index(i,_)) if !isval(t,boundLevel) =>
      env(i-boundLevel)

    case _ => throw new NoRuleApplies(t)
  }}

  def typeof(t:Term, ctx:List[(Name,Binding)]) = typeof1(t,ctx,Nil,0)
  def typeof(t:Term, ctx:List[(Name,Binding)],env:List[Term],boundLevel:Int):Term = typeof1(t,ctx,env,boundLevel)
  def typeof1(t:Term, ctx:List[(Name,Binding)],env:List[Term],boundLevel:Int):Term = t match {
    case TmVecElim(ty,m,mz,ms,k,list) =>
      val tyTy = evalType1(typeof1(ty, ctx,env, boundLevel),env,boundLevel)
      val mTy = evalType1(typeof1(m, ctx,env, boundLevel),env,boundLevel)
      val mzTy = evalType1(typeof1(mz, ctx,env, boundLevel),env,boundLevel)
      val msTy = evalType1(typeof1(ms, ctx,env, boundLevel),env,boundLevel)
      val kTy = evalType1(typeof1(k,ctx,env,boundLevel),env,boundLevel)
      val listTy = evalType1(typeof1(list, ctx,env, boundLevel),env,boundLevel)
      if (
        tyTy == Star &&
          mTy == TyPi("k",TyNat,TyPi("k",TyVec(indexShift(1,ty,0),Var(Index(0,"k"))),Star)) &&
          {/*println("mzTy = " + mzTy);*/mzTy} == {val a = evalType1(evalType1(App(App(m,TmNat(0)),TmNil(ty)),env,boundLevel),env,boundLevel);/*println("a = "+a)*/a} &&
          msTy ==
            evalType1(
              evalType1(
                TyPi("l",TyNat,TyPi("x",indexShift(1,ty,0),TyPi("xs",TyVec(indexShift(2,ty,0),Var(Index(1,"l"))),TyArr((App(App(indexShift(3,m,0),Var(Index(2,"l"))),Var(Index(0,"xs")))),App(App(indexShift(4,m,0),TmSucc(Var(Index(3,"l")))),TmCons(ty,Var(Index(3,"l")),Var(Index(2,"x")),Var(Index(1,"xs")))))))),
              env, boundLevel),
            env, boundLevel) &&
          kTy == TyNat &&
          listTy == TyVec(ty,k)
      )
        eval(App(App(m,k),list),env,boundLevel)
      //evalType1(eval(App(App(m,k),list),env,boundLevel),env,boundLevel)}
      else
        throw new TypeMismatchException("Ошибка типизации VecElim")

    case TmNatElim(q,mz,ms,l) =>
      val qTy  = eval(typeof1(q,  ctx, env, boundLevel), env, boundLevel)
      val mzTy = eval(typeof1(mz, ctx, env, boundLevel), env, boundLevel)
      val msTy = eval(typeof1(ms, ctx, env, boundLevel), env, boundLevel)
      val lTy  = eval(typeof1(l,  ctx, env, boundLevel), env, boundLevel)

      if (
        qTy  == TyArr(TyNat,Star) &&
          mzTy == eval(App(q,TmNat(0)),env,boundLevel) &&
          msTy == TyPi("_",TyNat,TyArr(eval(App(q,Var(Index(0,"_"))),env,boundLevel),eval(App(q,TmSucc(Var(Index(0,"_")))),env,boundLevel))) &&
          lTy  == TyNat
      )
        eval(App(q,l),env,boundLevel)
      else
        throw new TypeMismatchException("Ошибка типизации NatElim")

    case TmHead(ty,n,t1) =>
      val tyTy = typeof1(ty,ctx,env,boundLevel)
      val tyT1 = typeof1(t1,ctx,env,boundLevel)

      if (tyTy == Star)
        tyT1 match {
          case TyVec(ty1,n1) if ty1 == ty =>
            if (eval(TmSucc(n),env,boundLevel) == eval(n1,env,boundLevel))
              ty
            else
              throw new TypeMismatchException("Ошибка head: Неверный параметр длины списка")
          case TyVec(ty1,n1) =>
              throw new TypeMismatchException("Ошибка head: Тип "+ty+" не совпадает с типом "+ty1)
          case _ => throw new TypeMismatchException("Ошибка head: Операция head применима только к термам типа Vec")
        }
      else throw new TypeMismatchException("Ошибка head: "+ty+" не является типом")

    case TmTail(ty,n,t1) =>
      val tyTy = typeof1(ty,ctx,env,boundLevel)
      val tyT1 = typeof1(t1,ctx,env,boundLevel)

      if (tyTy == Star)
        tyT1 match {
          case TyVec(ty1,n1) if ty1 == ty =>
            if (eval(TmSucc(n),env,boundLevel) == eval(n1,env,boundLevel))
              TyVec(ty,eval(n,env,boundLevel))
            else
              //println("tail")
              throw new TypeMismatchException("Ошибка tail: Неверный параметр длины списка")
          case TyVec(ty1,_) =>
              throw new TypeMismatchException("Ошибка tail: Тип "+ty+" не совпадает с типом "+ty1)
        }
      else throw new TypeMismatchException("Ошибка tail: "+ty+" не является типом")

    case TmNil(ty) if typeof1(ty,ctx,env,boundLevel) == Star =>
      TyVec(ty,TmNat(0))
    case TmCons(ty,n,head,tail) =>
      val tyHead = typeof1(head,ctx,env,boundLevel)
      val tyTail = typeof1(tail,ctx,env,boundLevel)
      val tyN =typeof1(n,ctx,env,boundLevel)
      val vN = eval(n,env,boundLevel)
      if (tyHead == ty && tyN == TyNat)
        tyTail match {
          case TyVec(ty2,n2)  if ty2 == ty &&
            {eval(n2,env,boundLevel)}
              == vN =>
            TyVec(ty,eval(TmSucc(vN),env,boundLevel))
          case _ =>
            throw new TypeMismatchException("Ошибка cons: неверный параметр "+tail)
        }
      else
        throw new TypeMismatchException("Ошибка cons: Параметры типа и длины списка ["+ty+","+n+"] указаные неверно!")

    case (TmTrue | TmFalse | TmUnit) => (t.asInstanceOf[Const]).getType

    case TmIf(t1,t2,t3) =>
      if (TyBool ==  typeof1(t1, ctx,env, boundLevel)){
        val tyT2 =  typeof1(t2, ctx,env, boundLevel)
        if (tyT2 == typeof1(t3, ctx,env,boundLevel))
          tyT2
        else throw new TypeMismatchException("Ошибка if: обе альтернативы должны иметь один и тот же тип!")
      }
      else
        throw new TypeMismatchException("Ошибка if: условие должно иметь тип Bool")

    case Var(n) =>
      getTypeFromContext(n, ctx)

    case Lambda(body,ty,name) =>
      if (typeof1(ty,ctx,env,boundLevel) == Star)
        TyArr(ty, typeof1(body, (Index(0,name),VarBind(ty))::ctx,env,boundLevel+1))
      else
        throw new TypeMismatchException("Ошикба Lambda: "+ ty + " не является типом!")

    case TLambda(body,ty,name) =>
      if (typeof1(ty,ctx,env,boundLevel) == Star)
        TyPi(name,ty,typeof1(body,(Index(0,name),VarBind(ty))::ctx,env,boundLevel+1))
      else
        throw new TypeMismatchException("Ошикба TLambda: "+ ty + " не является типом!")

    case App(t1,t2) =>
      val tyT2 = evalType1(typeof1(t2,ctx,env, boundLevel),env,boundLevel)
      val tyT1 = typeof1(t1,ctx,env, boundLevel)
      tyT1 match {
        case TyArr(tyT11, tyT12) if evalType1(tyT11,env,boundLevel) == tyT2 =>
          indexShift(-1,tyT12,0)
        case TyPi(name,tyT11,tyT12) if evalType1(tyT11,env,boundLevel) == tyT2 => {
        val rr = substInType(indexShift(1,t2,0),0,tyT12)
          val r = indexShift(-1,rr,0)
          //val rrr = evalType1(r,env,boundLevel)
          r}
        case _ =>
          println("tyT1 = "+tyT1+ "tyT2 = "+tyT2+" eval(tyT1) = "+evalType1(tyT1,env,boundLevel))
          throw new TypeMismatchException("Ошибка App: применяемый терм должен иметь функциональный тип, а тип формального параметра должен совпадать с типом аргумента!")
      }
    case Let(name, t1, t2) =>
      val tyT1 = typeof1(t1,ctx,env,boundLevel)
      val tyT2 = typeof1(t2,(Index(0,name),VarBind(tyT1))::ctx,env++List(t1),boundLevel+1)
      tyT2

    case TmRecord(fields) =>
      TyRecord(fields.mapValues(t => typeof1(t,ctx,env,boundLevel)))

    case TmProj(t1,proj) =>
      typeof1(t1,ctx,env,boundLevel) match {
        case TyRecord(types) =>
          if (types.contains(proj))
            types(proj)
          else
            throw new TypeMismatchException("Ошибка ._"+proj+": запись не содержит поле "+proj)
        case _ => throw new TypeMismatchException("Ошибка ._"+proj+": проекция применима только к записи!")
      }

    case Fix(t1) =>
      typeof1(t1, ctx,env, boundLevel) match {
        case TyArr(t21,t22) if t21 == t22 =>
          t21
        case _ => throw new TypeMismatchException("Ошибка Fix")
      }

    case TmNat(_) =>
      TyNat

    case TmPred(t1) =>
      if (typeof1(t1,ctx,env,boundLevel) == TyNat)
        TyNat
      else throw new TypeMismatchException("Ошибка pred: аргумент должен иметь тип Nat!")

    case TmSucc(t1) =>
      if (typeof1(t1,ctx,env,boundLevel) == TyNat)
        TyNat
      else throw new TypeMismatchException("Ошибка succ: аргумент должен иметь тип Nat!")

    case TmIsZero(t1) =>
      if (typeof1(t1,ctx,env,boundLevel) == TyNat)
        TyBool
      else throw new TypeMismatchException("Ошибка isZero: аргумент должен иметь тип Nat!")

    case TyBool | TyNat | TyUnit | TyRecord(_) | Star => Star

    case TyArr(ty1,ty2) =>
      if (typeof1(ty1,ctx,env,boundLevel) == Star && typeof1(ty2,null::ctx,env,boundLevel) == Star)
        Star
      else
        throw new TypeMismatchException("Ошибка TyArr")

    case TyPi(name,ty1,ty2) =>
      if (typeof1(ty1,ctx,env,boundLevel) == Star && typeof1(ty2,(Index(0,name),VarBind(ty1))::ctx,env,boundLevel) == Star)
        Star
      else
        throw new TypeMismatchException("Ошибка Pi")

    case TyVec(ty,n) =>
      val tyTy = typeof1(ty,ctx,env,boundLevel)
      val tyN  = typeof1(n ,ctx,env,boundLevel)
      if (tyTy == Star && tyN == TyNat)
        Star
      else
        throw new TypeMismatchException("Ошибка Vec")

    case _ =>
      throw new TypeMismatchException("Неизвестный терм")
  }

  def evalType1(ty:Term,env:List[Term],boundLevel:Int):Term = ty match {
    case TyArr(ty1,ty2) =>
      TyArr(evalType1(ty1,env,boundLevel),evalType1(ty2,env,boundLevel+1))
    case TyVec(ty1,n) =>
      TyVec(evalType1(ty1,env,boundLevel),evalType1(n,env,boundLevel))
    case TyPi(name,ty1,ty2) =>
      TyPi(name,evalType1(ty1,env,boundLevel),evalType1(ty2,env,boundLevel+1))
    case _ =>
      eval(ty,env,boundLevel)
  }

  def getTypeFromContext(n:Name, ctx:List[(Name, Binding)]):Term = getTypeFromContext1(n,ctx,0)
  def getTypeFromContext1(n:Name,ctx:List[(Name, Binding)],index:Int):Term = ctx match {
    case (bn, VarBind(ty))::xs =>
      (n,bn) match {
        case (Index(i1,n1),Index(i2,n2)) =>
          if (i1-index == 0 && n1 == n2)
            indexShift(index+1, ty,0)
          else getTypeFromContext1(n,xs,index+1)
        case (Literal(n1),Literal(n2)) if n1 == n2 => ty
        case _ => getTypeFromContext1(n,xs,index+1)
      }
    case x::xs => getTypeFromContext1(n,xs,index+1)
    case Nil =>
      throw new TypeMismatchException("Тип переменной "+n+" не найден!")
  }

  def eval(t:Term,env:List[Term],boundLevel:Int):Term = {
    try {
      eval0(t,env,boundLevel)
    }
    catch {
      case e: NoRuleApplies => e.Result
    }
  }

  def eval0(t:Term,env:List[Term],boundLevel:Int):Term =
    eval0(eval1(t,env,boundLevel),env,boundLevel)
}