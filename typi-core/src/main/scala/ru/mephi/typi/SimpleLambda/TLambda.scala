package ru.mephi.typi.SimpleLambda

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:25
* To change this template use File | Settings | File Templates.
*/
case class TLambda(b:Term, ty:Term, name:String) extends Term {
  override def equals(other: Any) = other match {
    case TLambda(b2, ty2, _) =>  b == b2 && ty == ty2
    case _ => false
  }

  override def hashCode = 41 * (41 + b.hashCode()) + ty.hashCode()
}
