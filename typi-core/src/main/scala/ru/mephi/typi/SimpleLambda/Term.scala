package ru.mephi.typi.SimpleLambda

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 23.07.13
 * Time: 14:24
 * To change this template use File | Settings | File Templates.
 */
trait Term{
    override def toString = this match {
      case Var(x) => x match {
        case Index(i,name) => name+"("+i+")"
        case Literal(name) => name
      }
      case Lambda(body,ty,name) => "(Lambda "+name+":"+ty+"."+body+")"
      case TLambda(body,ty,name) => "(TLambda "+name+":"+ty+"."+body+")"
      case App(t1,t2) => "("+t1+" "+t2+")"
      case Let(name, t1, t2) => "let "+name+"="+t1+" in "+t2
      case TmTrue => "true"
      case TmFalse => "false"
      case TmIf(t1,t2,t3) => "(if "+t1+" then "+t2+" else "+t3+")"
      case TmUnit => "unit"
      case Fix(t) => "fix "+ t
      case TmPred(t) => "(pred "+t+")"
      case TmHead(ty,n,list) => "head["+ty+","+n+"]("+list+")"
      case TmTail(ty,n,list) => "tail["+ty+","+n+"]("+list+")"
      case TmNatElim(m,mz,ms,l) => "TmNatElim("+m+","+mz+","+ms+","+l+")"
      case TmVecElim(ty,m,mz,ms,l,list) =>
        "TmVecElim(\n" +
        "  "+ty+",\n"+
        "  "+m+",\n" +
        "  "+mz+",\n"+
        "  "+ms+",\n"+
        "  "+l+",\n" +
        "  "+list+"\n" +
        ")"
      case _ => "Добавь новые правила!"
    }


    def eval:Term = SimpleLambda.eval(this,Nil,0)
    def eval(env:List[Term]):Term = SimpleLambda.eval(this,env,0)
    def eval(env:List[Term],boundLevel:Int) = SimpleLambda.eval(this,env,boundLevel)
    //def evalImp:term = SimpleLambda.evalImp(this)
    def typeof(ctx:List[(Name,Binding)]):Term =  SimpleLambda.typeof(this,ctx)
  }
