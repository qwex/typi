package ru.mephi.typi.SimpleLambda

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:25
* To change this template use File | Settings | File Templates.
*/
case class TmRecord(fields:Map[String,Term]) extends Term {
  override def toString = toString1("{",fields.toList)

  def toString1(acc:String,rest:List[(String,Term)]):String = rest match {
    case x::y::xs => toString1(acc+x._1+" = "+x._2+", ",y::xs)
    case y::Nil => acc + y._1 + " = " + y._2+"}"
  }
}
