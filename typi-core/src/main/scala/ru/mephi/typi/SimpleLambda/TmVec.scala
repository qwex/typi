package ru.mephi.typi.SimpleLambda

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:28
* To change this template use File | Settings | File Templates.
*/
trait TmVec extends Term {
    override def toString = this match {
      case TmCons(ty,n,head,tail) =>  "cons[ "+ty+", "+n+"]( "+head+", "+tail+")"
      case TmNil(ty) => "Nil["+ty+"]"
    }

    //def toString1 = this match {
    //  case TmNil => ""
    //  case TmCons(ty,n,head,body)
    //}
}
