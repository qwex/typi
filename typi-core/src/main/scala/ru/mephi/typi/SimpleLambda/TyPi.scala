package ru.mephi.typi.SimpleLambda

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:31
* To change this template use File | Settings | File Templates.
*/
case class TyPi(name:String,ty1:Term,ty2:Term) extends Type  {
  override def equals(other: Any) = other match {
    case TyPi(_,ty12,ty22) =>  ty1 == ty12 && ty2 == ty22
    case _ => false
  }

  override def hashCode = 41 * (41 + ty1.hashCode()) + ty2.hashCode()
}
