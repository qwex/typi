package ru.mephi.typi.SimpleLambda

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:27
* To change this template use File | Settings | File Templates.
*/
trait Type extends Term{
  override def toString = this match {
    case TyBool => "Bool"
    case TyUnit => "Unit"
    case TyNat => "Nat"
    case TyArr(ty1,ty2) => "("+ty1+"->"+ty2+")"
    case TyPi(name,ty1,ty2) => "(Pi "+name+":"+ty1+"."+ty2+")"
    case Var(_) => super.toString
    case TyVec(ty,n) => "Vec( "+ty+", "+n+")"
    case _ => "Я не знаю как выводить этот тип!"
  }
}
