package ru.mephi.typi.core

import term._

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:34
* To change this template use File | Settings | File Templates.
*/
trait Const extends Term {
  def getType:Term = this match {
    case TmTrue | TmFalse => TyBool
    case TmUnit => TyUnit
  }
  override def isval(boundLevel: Int): Boolean = true
}
