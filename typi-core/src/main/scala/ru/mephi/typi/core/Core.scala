package ru.mephi.typi.core

import term.Term


/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 03.09.13
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */
object Core {
  def eval1(t:Term,env:List[Term],boundLevel:Int) =
    t.eval1(env,boundLevel)

  def isval(t:Term,boundLevel:Int) =
    t.isval(boundLevel)

  def subst(t1:Term,i:Int,t2:Term) =
    t2.subst(t1,i)
}
