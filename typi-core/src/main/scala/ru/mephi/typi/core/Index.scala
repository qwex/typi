package ru.mephi.typi.core

import exception.TypeMismatchException
import term.Term

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
case class Index(i:Int, name:String) extends Name {
  override def getTypeFromContext(ctx:List[(Name,Binding)], index:Int):Term = ctx match {
    case (boundName, VarBind(ty))::xs =>
      boundName match {
        case Index(i2,name2) =>
          if (i-index == 0 && name == name2)
            ty.indexShift(index+1,0)
          else
            this.getTypeFromContext(xs,index+1)
        case _ => this.getTypeFromContext(xs,index+1)
      }
    case x::xs =>
      this.getTypeFromContext(xs,index+1)
    case Nil =>
      throw new TypeMismatchException("Тип переменной "+name+" не найден! "+ ctx)
  }

  override def equals(other: Any) = other match {
    case Index(i2, _) =>  i == i2
    case _ => false
  }

  override def hashCode = 41 * (41 + i.hashCode())

  override def toString() =
    name+"("+i+")"
}
