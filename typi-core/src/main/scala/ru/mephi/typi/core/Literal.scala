package ru.mephi.typi.core

import exception.TypeMismatchException
import term.Term

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */
case class Literal(name:String) extends Name {
  override def getTypeFromContext(ctx:List[(Name,Binding)], index:Int):Term = ctx match {
    case (boundName, VarBind(ty))::xs =>
      boundName match {
        case Literal(n2) if name == n2 => ty
        case _ => this.getTypeFromContext(xs,index+1)
      }
    case x::xs =>
      this.getTypeFromContext(xs,index+1)
    case Nil =>
      throw new TypeMismatchException("Тип переменной "+name+" не найден!")
  }

  override def toString() =
   name
}
