package ru.mephi.typi.core

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */

import term.Term

trait Name {
  def getTypeFromContext(ctx:List[(Name, Binding)]):Term = this.getTypeFromContext(ctx,0)

  def getTypeFromContext(ctx:List[(Name,Binding)], index:Int):Term =
    throw new NotImplementedError()
}
