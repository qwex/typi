package ru.mephi.typi.core

import term.Term

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
case class VarBind(ty:Term) extends Binding {
  override def toString = ty.toString
}
