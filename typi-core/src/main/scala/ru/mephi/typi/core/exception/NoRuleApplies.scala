package ru.mephi.typi.core.exception

import ru.mephi.typi.core.term.Term

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 03.09.13
 * Time: 22:42
 * To change this template use File | Settings | File Templates.
 */
class NoRuleApplies(t:Term) extends Exception {
  def Result = t
}
