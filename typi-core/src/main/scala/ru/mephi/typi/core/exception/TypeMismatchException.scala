package ru.mephi.typi.core.exception

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 12.07.13
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */
class TypeMismatchException(val msg:String) extends Exception(msg)
