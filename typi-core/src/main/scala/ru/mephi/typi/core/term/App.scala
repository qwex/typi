package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import ru.mephi.typi.core.exception.{TypeMismatchException, NoRuleApplies}
import collection.immutable.HashMap


/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 17:11
 * To change this template use File | Settings | File Templates.
 */
case class App(t1:Term,t2:Term) extends Term {
  override def eval1(env:List[Term],boundLevel:Int):Term = (t1,t2) match {
    case (TLambda(body,ty,name), t2) =>
      App(Lambda(body, ty, name),t2)
    case (Lambda(t1,_,_), v2) if v2 isval boundLevel =>
      (t1.subst(v2.indexShift(1,0), 0)).indexShift(-1,0)
    case (v1,v2) if (v1 isval boundLevel) && (v2 isval boundLevel) =>
      throw new NoRuleApplies(this)
    case (v1,t2) if  v1.isval(boundLevel) =>
      App(v1,t2.eval1(env,boundLevel))
    //case (v1,UnknownType(_)) =>
    //  v1
    case (t1,t2) =>
      App(t1.eval1(env,boundLevel),t2)
  }

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int) = {
    val tyT2 = t2.typeof1(ctx, env, boundLevel).evalType1(env,boundLevel)
    val tyT1 = t1.typeof1(ctx, env, boundLevel)

    tyT1 match {
      case TyArr(tyT11, tyT12) if tyT11.evalType1(env, boundLevel) == tyT2 =>
        tyT12.indexShift(-1, 0)
      case TyArr(tyT11,tyT12) =>
        val s = tyT11.evalType1(env,boundLevel).unify(tyT2, boundLevel)
        println("Подстановка: "+s, s.indexShift(1)(tyT12).indexShift(-1,0), s.indexShift(0)(tyT12).indexShift(-1,0))
        s.indexShift(1)(tyT12).indexShift(-1,0)
      case TyPi(name,tyT11,tyT12) if tyT11.evalType1(env,boundLevel) == tyT2 => {
        val rr = tyT12.substInType(t2.indexShift(1,0),0)
        val r = rr.indexShift(-1,0)
        r.evalType1(env,boundLevel)}
      case _ =>
        //println("tyT1 = "+tyT1+ " tyT2 = "+tyT2+" eval(tyT1) = "+tyT1.evalType1(env,boundLevel) + " t1 = "+t1 + " t2 = "+t2)
        throw new TypeMismatchException("Ошибка App: применяемый терм должен иметь функциональный тип, а тип формального параметра должен совпадать с типом аргумента!")
    }
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    App(t1.subst(substTerm, i),t2.subst(substTerm, i))

  override def isval(boundLevel:Int):Boolean = (t1, t2) match {
    case (Lambda(_,_,_),t1) => false
    case (v1 @ Var(_),v2) if v1.isval(boundLevel) && v2.isval(boundLevel) => true
    case (t1,t2) => t1.isval(boundLevel) && t2.isval(boundLevel)
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    App(t1.indexShift(shift,shiftBound), t2.indexShift(shift,shiftBound))

  override def toString() = {
    "("+t1+" "+t2+")"
  }
}

trait TSubst {
  def apply(t:Term):Term
  def indexShift(i:Int):TSubst
}

case object EmptySubst extends TSubst {
   def apply(t:Term):Term = t
   def indexShift(i:Int) = this
}

case class ComposeSubst(s1:TSubst,s2:TSubst) extends TSubst {
  def apply(t:Term):Term = s1(s2(t))
  def indexShift(i:Int) = {
    ComposeSubst(s1.indexShift(i), s2.indexShift(i)) }
}

case class SUBST(subst:Map[Term,Term]) extends TSubst{
  def apply(t:Term):Term = t match {
    case TyArr(ty1,ty2) =>
      TyArr(this(ty1),this.indexShift(1)(ty2))
    case TyPi(n,ty1,ty2) =>
      TyPi(n,this(ty1),this.indexShift(1)(ty2))
    case v @ Var(_) =>
      if (subst.contains(v))
        subst(v)
      else
        v
    case v @ UnknownType(_,name) =>
      if (subst.contains(v))
        subst(v)
      else
        v
    case _ => t
  }

  def indexShift(shift:Int):SUBST = {
    SUBST(for ((k,v)<-subst)yield {
      (k match {
        case UnknownType(n,name) => UnknownType(n+shift,name)
        case _ => k
      }, v.indexShift(shift,0))
      })
  }
}
