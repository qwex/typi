package ru.mephi.typi.core.term

import ru.mephi.typi.core.{Binding, Name, Const}

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 06.09.13
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */
trait Bool extends Const {
  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term =
    TyBool
}
