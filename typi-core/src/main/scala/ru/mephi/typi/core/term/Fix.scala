package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}


/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 03.09.13
 * Time: 22:13
 * To change this template use File | Settings | File Templates.
 */
case class Fix(t:Term) extends Term {
  override def eval1(env:List[Term],boundLevel:Int):Term = t match {
    case v if v.isval(boundLevel) =>
      v match {
        case Lambda(body, _, _) =>
          body.subst(this.indexShift(1, boundLevel),0).indexShift(-1, boundLevel)
        case _ => throw new NoRuleApplies(this)
      }
    case t =>
      Fix(t.eval1(env,boundLevel))
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    Fix(t.subst(substTerm,i))

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    t.typeof1(ctx,env, boundLevel) match {
      case TyArr(t21,t22) if t21 == t22 =>
        t21
      case _ => throw new TypeMismatchException("Ошибка Fix")
    }
  }

  override def indexShift(shift:Int, shiftBound:Int):Term = {
    Fix(t.indexShift(shift, shiftBound))
  }

  override def toString() =
    "fix "+ t
}
