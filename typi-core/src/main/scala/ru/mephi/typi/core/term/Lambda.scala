package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}
import ru.mephi.typi.core.Index

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */
case class Lambda(b:Term, ty:Term,name:String) extends Term {
  //Интерфейс
  override def eval1(env:List[Term],boundLevel:Int):Term =
    throw new  NoRuleApplies(this)

  override def typeof1(ctx:List[(Name,Binding)],env:List[Term],boundLevel:Int):Term =
    if (ty.typeof1(ctx,env,boundLevel) == Star) {
      //println("aaaaa"+ty)
      val eTy = ty.evalType1(env, boundLevel)
      //println("|||| "+this + " "+boundLevel+" "+eTy)
      //println("BBBB"+eTy)
      TyArr(eTy, b.typeof1((Index(0,name), VarBind(eTy))::ctx, env, boundLevel+1))
    }
    else
      throw new TypeMismatchException("Ошибка Lambda: "+ ty + " не является типом!")

  override def isval(boundLevel:Int) = true

  override def indexShift(shift:Int, shiftBound:Int):Term =
    Lambda(b.indexShift(shift,shiftBound+1), ty.indexShift(shift,shiftBound),name)


  override def substInType(substTerm: Term, i: Int): Term =
    Lambda(b.substInType(substTerm.indexShift(1,0),i+1), ty.substInType(substTerm, i), name)

  override def subst(substTerm:Term,i:Int):Term =
    Lambda(b.subst (substTerm.indexShift(1,0), i+1),ty.substInType(substTerm, i),name)

  //Собственные методы
  override def equals(other: Any) = other match {
    case Lambda(b2, ty2, _) =>  b == b2 && ty == ty2
    case _ => false
  }

  override def hashCode = 41 * (41 + b.hashCode()) + ty.hashCode()

  override def toString() = {
    "(Lambda "+name+":"+ty+"."+b+")"
  }
}
