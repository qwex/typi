package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import ru.mephi.typi.core.Index

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 03.09.13
 * Time: 22:14
 * To change this template use File | Settings | File Templates.
 */
case class Let(name:String, t1:Term, t2:Term) extends Term {
   override def eval1(env:List[Term],boundLevel:Int) = (t1,t2) match {
     case (v1, t2) if v1.isval(boundLevel) =>
       t2.subst(v1.indexShift(1,0),0).indexShift(-1,0)
       //t2.subst(v1, 0).eval1(env, boundLevel+1)
     case (t1, t2) =>
       Let(name, t1.eval1(env,boundLevel), t2)
   }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    Let(name, t1.subst(substTerm, i), t2.subst(substTerm, i+1))

   override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
     val tyT1 = t1.typeof1(ctx,env,boundLevel)
     val tyT2 = t2.typeof1((Index(0,name),VarBind(tyT1))::ctx,env/*++List(t1)*/,boundLevel/*+1*/)
     tyT2
   }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    Let(name, t1.indexShift(shift, shiftBound), t2.indexShift(shift, shiftBound+1))

  override def toString() = "let "+name+"="+t1+" in "+t2
}
