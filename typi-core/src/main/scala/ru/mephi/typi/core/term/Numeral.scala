package ru.mephi.typi.core.term


/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:26
* To change this template use File | Settings | File Templates.
*/
trait Numeral extends Term {
  override def toString = toString1(this,0)

  def toString1(t:Term,acc:Int):String = t match {
    case TmSucc(t1) => t1 match {
      //case TmNat(i) => (i + acc+1).toString
      case _ => "(succ " + t1+")"
    }
    case TmNat(i) => (i + acc).toString
    case TmPred(t1) => "(pred " + t1+")"
    case TmIsZero(t1) => "(iszero " + t1+")"
  }
}
