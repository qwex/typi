package ru.mephi.typi.core.term

import ru.mephi.typi.core.{Binding, Name, Const}
import ru.mephi.typi.core.exception.NoRuleApplies


/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 16:31
 * To change this template use File | Settings | File Templates.
 */
case object Star extends Const with Type {
  //Интерфейс
  override def eval1(env:List[Term],boundLevel:Int):Term =
    throw new NoRuleApplies(this)

  override def typeof1(ctx:List[(Name,Binding)],env:List[Term],boundLevel:Int) = this

  override def isval(boundLevel:Int) = true
  //Собственные методы
  override def toString = "*"
}
