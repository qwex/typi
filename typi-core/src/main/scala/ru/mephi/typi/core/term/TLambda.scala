package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.TypeMismatchException
import ru.mephi.typi.core.Index

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
case class TLambda(b:Term, ty:Term, name:String) extends Term {
  //Интерфейс
  override def eval1(env:List[Term], boundLevel:Int):Term =
    Lambda(b, ty, name)

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    if (ty.typeof1(ctx,env,boundLevel) == Star)
      TyPi(name, ty, b.typeof1((Index(0,name),VarBind(ty))::ctx,env,boundLevel+1))
    else
      throw new TypeMismatchException("Ошикба TLambda: "+ ty + " не является типом!")
  }

  override def isval(boundLevel:Int) = false

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TLambda(b.subst(substTerm.indexShift(1,0), i+1),ty.substInType(substTerm, i),name)

  //Собственные методы
  override def equals(other: Any) = other match {
    case TLambda(b2, ty2, _) =>  b == b2 && ty == ty2
    case _ => false
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TLambda(b.indexShift(shift,shiftBound+1), ty.indexShift(shift,shiftBound),name)

  override def hashCode = 41 * (41 + b.hashCode()) + ty.hashCode()

  override def toString() = {
    "(TLambda "+name+":"+ty+"."+b+")"
  }
}
