package ru.mephi.typi.core.term

import ru.mephi.typi.core.exception.{TypeMismatchException, NoRuleApplies}
import ru.mephi.typi.core.{Binding, Name}
import ru.mephi.typi.core.Index

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 16:23
 * To change this template use File | Settings | File Templates.
 */
trait Term {
  def eval1(env:List[Term],boundLevel:Int):Term =
    throw new NoRuleApplies(this)
  def evalType1(env:List[Term], boundLevel:Int):Term =
    eval(env, boundLevel)

  def typeof = typeof1(Nil,Nil,0)
  def typeof(ctx:List[(Name,Binding)]) =
    this.typeof1(ctx,Nil,0)
  def typeof(ctx:List[(Name,Binding)],env:List[Term],boundLevel:Int):Term =
    this.typeof1(ctx,env,boundLevel)
  def typeof1(ctx:List[(Name,Binding)],env:List[Term],boundLevel:Int):Term =
    throw new TypeMismatchException("")

  def indexShift(shift:Int ,shiftBound:Int):Term = this

  def subst(substTerm:Term,i:Int):Term =
    this.generalSubst(substTerm, i, this.subst)
  def substInType(substTerm:Term, i:Int):Term = this.generalSubst(substTerm,i,this.substInType)

  def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term = this

  def isval(boundLevel:Int):Boolean = this.isnumeral//false?

  def isnumeral:Boolean =false

  /*def eval0(env:List[Term],boundLevel:Int):Term =
    this.eval1(env,boundLevel).eval0(env,boundLevel)*/

  var results:List[Term] = Nil

  /*def eval3(env:List[Term], boundLevel:Int):Term = {
    if (this.isval(boundLevel))
      this
    else
      this.eval1(env, boundLevel).eval3(env,boundLevel)
  }*/

  /*def eval2(env:List[Term], boundLevel:Int):Term = {
    val result = this.eval1(env, boundLevel)
    results = result::results;
    result.results = this.results;
    result.eval2(env,boundLevel)
  }*/

  def eval(env:List[Term], boundLevel:Int, steps:List[Term]):List[Term] =
  {
    val result =
      try {
        eval1(env,boundLevel)//this.eval3(env,boundLevel)
      }
      catch {
        case e: NoRuleApplies =>
          return steps
         //{println("Я вышел здесь");e.Result}

      }
    result.eval(env,boundLevel,result::steps)
  }

  def eval(env:List[Term], boundLevel:Int):Term = eval(env, boundLevel, List(this)).head

  def eval(env:List[Term]):Term = this.eval(env,0)
  def eval:Term = this.eval(Nil,0)

  def unify(t:Term, boundLevel:Int):TSubst = (this, t) match {
    //case (v1@Var(_),v2@Var(_ )) if v1 == v2 =>
    //case (v1@Var(_),v2@Var(_ )) if v1 == v2 =>
    //  EmptySubst
    //case (v@Var(_), t) =>
    //case (v@UnknownType(n), Var(Index(i,name))) =>
    //  println("name = "+name + " boundLevel = "+boundLevel)
    //  SUBST(Map((v,Var(Index(n/*+boundLevel*/,name)))))
    case (v@UnknownType(i, n), t) =>
        SUBST(Map((v,t)))
    //case (t, v@ UnknownType(_))=>
    //   SUBST(Map((v,t)))
    case (TyArr(ty1,ty2),TyArr(ty3,ty4))=>
      val s = ty1.unify(ty3, boundLevel)
      val ss = ComposeSubst(s.indexShift(1)(ty2).unify(s.indexShift(1)(ty4), boundLevel).indexShift(-1),s)
      println("ssss: ", s, ss,TyArr(ty1,ty2),TyArr(ty3,ty4));
      ss
    case (TyPi(_,ty1,ty2),TyPi(_,ty3,ty4))=>
      val s = ty1.unify(ty3, boundLevel)
      ComposeSubst(s.indexShift(1)(ty2).unify(s.indexShift(1)(ty4), boundLevel).indexShift(-1),s)
    case (ty1,ty2) if ty1 == ty2 =>
      EmptySubst
    case _ =>
      throw new TypeMismatchException("Невозможно вывести параметр типа. "+" this = "+this+" t = "+t)
  }
}
