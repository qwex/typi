package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:29
* To change this template use File | Settings | File Templates.
*/
case class TmCons(ty:Term,n:Term,head:Term,tail:Term) extends TmVec {
  override def eval1(env:List[Term], boundLevel:Int) = (ty, n, head, tail) match {
    case (_,_,_,_) if this.isval(boundLevel) =>
      throw new NoRuleApplies(this)
    case (ty,vn,vhead,tail) if (vn.isval(boundLevel) && vhead.isval(boundLevel)) =>
      TmCons(ty,vn,vhead,tail.eval1(env,boundLevel))
    case (ty,vn,head,tail) if vn.isval(boundLevel) =>
      TmCons(ty,vn,head.eval1(env,boundLevel),tail)
    case (ty,n,head,tail) =>
      TmCons(ty,n.eval1(env,boundLevel),head,tail)
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmCons(ty.substInType(substTerm,i),n.subst(substTerm,i),head.subst(substTerm,i),tail.subst(substTerm,i))

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
      val tyHead = head.typeof1(ctx, env, boundLevel)
      val tyTail = tail.typeof1(ctx, env, boundLevel)
      val tyN    =    n.typeof1(ctx, env, boundLevel)
      val vN     = n.eval(env, boundLevel)
      var r:Term = head
      if (tyHead == ty && tyN == TyNat)
        tyTail match {
          case TyVec(ty2,n2)  if ty2 == ty &&
            {r = n2.eval(env, boundLevel);r}
              == vN =>
            TyVec(ty,TmSucc(vN).eval(env,boundLevel))
          case _ =>
            throw new TypeMismatchException("Ошибка cons: неверный параметр "+tail+" Число n: "+vN.getClass+" Число n2: "+ r.getClass)
        }
      else
        throw new TypeMismatchException("Ошибка cons: Параметры типа и длины списка ["+ty+","+n+"] указаные неверно!")
  }


  override def isval(boundLevel: Int): Boolean = n.isval(boundLevel) && head.isval(boundLevel) && tail.isval(boundLevel)

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TmCons(ty.indexShift(shift,shiftBound),n.indexShift(shift,shiftBound),head.indexShift(shift,shiftBound),tail.indexShift(shift,shiftBound))

  override def toString = "cons[ "+ty+", "+n+"]( "+head+", "+tail+")"
}
