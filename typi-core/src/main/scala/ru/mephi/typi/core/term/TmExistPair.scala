package ru.mephi.typi.core.term

import ru.mephi.typi.core.{Binding, Name}
import ru.mephi.typi.core.exception.{TypeMismatchException, NoRuleApplies}

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 18.11.13
 * Time: 16:50
 * To change this template use File | Settings | File Templates.
 */
case class TmExistPair(t1:Term, t2:Term, ann:Term) extends Term {
  override def eval1(env:List[Term], boundLevel:Int):Term =
    if (this.isval(boundLevel))
      throw new NoRuleApplies(this)
    else if (t1.isval(boundLevel))
      TmExistPair(t1, t2.eval1(env, boundLevel), ann)
    else
      TmExistPair(t1.eval(env, boundLevel), t2, ann)

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    val tyT1 = t1.typeof(ctx, env, boundLevel)
    ann match {
      case TySigma(name,t11, t12) =>
        if (tyT1 == t11.evalType1(env, boundLevel)) {
          val tyT12 = t12.substInType(t1, 0).evalType1(env, boundLevel+1)
          if (tyT12.typeof1(ctx, env, boundLevel) == Star
            && t2.typeof(ctx, env, boundLevel) == tyT12)
            TySigma(name, tyT1, t12)
          else
            throw new TypeMismatchException("Ошибка!")
        }
        else
          throw new TypeMismatchException("Ошибка экзистенционального типа!")
      case _ =>
        throw new TypeMismatchException("Экзистенциональная пара должна иметь экзистенциональный тип!")
    }
    //if (t1.typeof1(ctx,env,boundLevel) == Star)
    //  TyPi(name, ty, b.typeof1((Index(0,name),VarBind(ty))::ctx,env,boundLevel+1))
    //else
    //  throw new TypeMismatchException("Ошикба TLambda: "+ ty + " не является типом!")
  }

  override def isval(boundLevel:Int) = t1.isval(boundLevel) && t2.isval(boundLevel)

  override def substInType(substTerm:Term, i:Int) =
    TmExistPair(t1, t2, ann.substInType(substTerm, i))

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmExistPair(t1.generalSubst(substTerm, i, subst), t2.generalSubst(substTerm, i, subst), ann)

  //Собственные методы
  override def equals(other: Any) = other match {
    case TmExistPair(t21, t22, ann2) =>  t1 == t21 && t2 == t22 && ann == ann2
    case _ => false
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TmExistPair(t1.indexShift(shift, shiftBound), t2.indexShift(shift, shiftBound), ann.indexShift(shift, shiftBound))

  override def hashCode = 41 * (41 * ((41 + t1.hashCode()) + t2.hashCode()) + ann.hashCode())

  override def toString() = "({"+t1+","+t2+"} as "+ann+")"
}
