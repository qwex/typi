package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.TypeMismatchException

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:29
* To change this template use File | Settings | File Templates.
*/
case class TmHead(ty:Term,n:Term,t:Term) extends Term {
  override def eval1(env:List[Term], boundLevel:Int) = t match {
    case TmCons(ty,n,head,tail) => tail
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmHead(ty.substInType(substTerm,i), n.subst(substTerm,i), t.subst(substTerm,i))

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int) = {
      val tyTy = ty.typeof1(ctx,env,boundLevel)
      val tyT  =  t.typeof1(ctx,env,boundLevel)

      if (tyTy == Star)
        tyT match {
          case TyVec(ty1,n1) if ty1 == ty =>
            if (TmSucc(n).eval(env,boundLevel) == n1.eval(env,boundLevel))
              ty
            else
              throw new TypeMismatchException("Ошибка head: Неверный параметр длины списка")
          case TyVec(ty1,n1) =>
            throw new TypeMismatchException("Ошибка head: Тип "+ty+" не совпадает с типом "+ty1)
          case _ => throw new TypeMismatchException("Ошибка head: Операция head применима только к термам типа Vec")
        }
      else throw new TypeMismatchException("Ошибка head: "+ty+" не является типом")
  }

  override def toString() =
    "head["+ty+","+n+"]("+t+")"
}
