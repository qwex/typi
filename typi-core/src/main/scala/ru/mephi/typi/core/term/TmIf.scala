package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{NoRuleApplies, TypeMismatchException}

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 06.09.13
 * Time: 13:36
 * To change this template use File | Settings | File Templates.
 */
case class TmIf(t1:Term,t2:Term,t3:Term) extends Term {
  override def eval1(env:List[Term],boundLevel:Int):Term =
    if (t1.isval(boundLevel)) t1 match {
      case TmTrue  => t2
      case TmFalse => t3
      case _ => throw new NoRuleApplies(this)
    } else
      TmIf(t1.eval1(env,boundLevel),t2,t3)


  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmIf(t1.subst(substTerm,i), t2.subst(substTerm,i), t3.subst(substTerm,i))

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int) = {
    if (TyBool ==  t1.typeof1(ctx,env, boundLevel)){
      val tyT2 = t2.typeof1(ctx,env, boundLevel)
      if (tyT2 == t3.typeof1(ctx,env,boundLevel))
        tyT2
      else
        throw new TypeMismatchException("Ошибка if: обе альтернативы должны иметь один и тот же тип!")
    }
    else
      throw new TypeMismatchException("Ошибка if: условие должно иметь тип Bool")
  }

  override def indexShift(shift:Int, shiftBound:Int) =
    TmIf(t1.indexShift(shift,shiftBound), t2.indexShift(shift, shiftBound), t3.indexShift(shift, shiftBound))

  override def toString() =
    "(if "+t1+" then "+t2+" else "+t3+")"
}
