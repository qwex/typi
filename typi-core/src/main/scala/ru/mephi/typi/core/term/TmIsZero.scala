package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{NoRuleApplies, TypeMismatchException}

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:26
* To change this template use File | Settings | File Templates.
*/
case class TmIsZero(t:Term) extends Numeral {
  override def eval1(env:List[Term], boundLevel:Int) = t match {
    case TmNat(n) => if (n == 0)
      TmTrue else TmFalse
    case TmSucc(t1) /*if t1.isnumeral*/ =>
      TmFalse
    case _ if !t.isval(boundLevel)=>
      TmIsZero(t.eval1(env,boundLevel))
    case _ => throw new NoRuleApplies(this)
  }

  override def isval(boundLevel:Int) = t match {
    case Var(_)  => t.isval(boundLevel)
    case _ => false
  }

  override def indexShift(shift:Int, shiftBound:Int) =
    TmIsZero(t.indexShift(shift, shiftBound))

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmIsZero(t.subst(substTerm,i))

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
      if (t.typeof1(ctx,env,boundLevel) == TyNat)
        TyBool
      else throw new TypeMismatchException("Ошибка isZero: аргумент должен иметь тип Nat!")
  }
}
