package ru.mephi.typi.core.term

import ru.mephi.typi.core._

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:26
* To change this template use File | Settings | File Templates.
*/
case class TmNat(i:Int) extends Numeral {
  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term =
    TyNat

  override def isnumeral: Boolean = true
}
