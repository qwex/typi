package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}
import ru.mephi.typi.core.Index

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:27
* To change this template use File | Settings | File Templates.
*/
case class TmNatElim(m:Term,mz:Term,ms:Term,l:Term) extends Term {
  override def eval1(env:List[Term], boundLevel:Int) = (m, mz, ms, l) match {
    case (vm,vmz,vms,vl) if List(vm,vmz,vms,vl).map(v=>v.isval(boundLevel)).fold(true)(_ && _) =>
      vl match {
        case TmNat(n) if n == 0 => vmz
        case TmNat(n) => App(App(vms,TmNat(n-1)),TmNatElim(vm,vmz,vms,TmNat(n-1)))
        case TmSucc(l) => App(App(vms,l),TmNatElim(vm,vmz,vms,l))
        case _ => throw new NoRuleApplies(this)
      }
    case (vm,vmz,vms,l) if List(vm,vmz,vms).map(v=>v.isval(boundLevel)).fold(true)(_ && _) =>
      TmNatElim(vm,vmz,vms,l.eval1(env,boundLevel))
    case (vm,vmz,ms,l) if List(vm,vmz).map(v=>v.isval(boundLevel)).fold(true)(_ && _) =>
      TmNatElim(vm,vmz,ms.eval1(env,boundLevel),l)
    case (vm,mz,ms,l) if vm.isval(boundLevel) =>
      TmNatElim(vm,mz.eval1(env,boundLevel),ms,l)
    case (m,mz,ms,l) =>
      TmNatElim(m.eval1(env,boundLevel),mz,ms,l)
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TmNatElim(m.indexShift(shift,shiftBound),mz.indexShift(shift,shiftBound),ms.indexShift(shift,shiftBound),l.indexShift(shift,shiftBound))

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmNatElim(m.subst(substTerm,i), mz.subst(substTerm,i), ms.subst(substTerm,i), l.subst(substTerm,i))


  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
      val mTy  =  m.typeof1(ctx, env, boundLevel).eval(env, boundLevel)
      val mzTy = mz.typeof1(ctx, env, boundLevel).eval(env, boundLevel)
      val msTy = ms.typeof1(ctx, env, boundLevel).eval(env, boundLevel)
      val lTy  =  l.typeof1(ctx, env, boundLevel).eval(env, boundLevel)

      //println(m)
      //println(boundLevel)
      //println("||||||||" +TyPi("_",TyNat,TyArr(App(m,Var(Index(0,"_"))).eval(env,boundLevel+1),App(m.indexShift(2,0),TmSucc(Var(Index(0,"_")))).eval(env,boundLevel+2))) )
    val r = TyPi("_",TyNat,TyArr(App(m,Var(Index(0,"_"))),App(m.indexShift(2,0),TmSucc(Var(Index(0,"_"))))))
    //println(m.indexShift(1,0))
    //println(r);
    //println(App(m.indexShift(1,0),Var(Index(0,"_"))).eval(env,boundLevel+1))
    //  println("||||+++++" +TyPi("_",TyNat,TyArr(App(m.indexShift(1,0),Var(Index(0,"_"))).eval(env,boundLevel+1),App(m.indexShift(1,0),TmSucc(Var(Index(0,"_")))).eval(env,boundLevel+1))) )

      if (
        mTy  == TyArr(TyNat,Star) &&
          mzTy == App(m,TmNat(0)).eval(env,boundLevel) &&
          msTy == TyPi("_",TyNat,TyArr(App(m.indexShift(1,0),Var(Index(0,"_"))).eval(env,boundLevel+1),App(m.indexShift(1,0),TmSucc(Var(Index(0,"_")))).eval(env,boundLevel+1)))
            /*TyPi("_",TyNat,TyArr(App(m,Var(Index(0,"_"))).eval(env,boundLevel+1),App(m,TmSucc(Var(Index(0,"_")))).eval(env,boundLevel+2)))*/ &&
          lTy  == TyNat
      )
        App(m,l).eval(env,boundLevel)
      else
        throw new TypeMismatchException("Ошибка типизации NatElim")
  }

  override def isval(boundLevel: Int): Boolean = l match {
    case TmSucc(l) => false
    case _ =>
      !l.isnumeral && m.isval(boundLevel) && mz.isval(boundLevel) && ms.isval(boundLevel) && l.isval(boundLevel)
  }

  override def toString() =
    "TmNatElim("+m+","+mz+","+ms+","+l+")"
}
