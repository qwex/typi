package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.TypeMismatchException

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:29
* To change this template use File | Settings | File Templates.
*/
case class TmNil(ty:Term) extends TmVec {
  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    if (ty.typeof1(ctx,env,boundLevel) == Star)
      TyVec(ty,TmNat(0))
    else
      throw new TypeMismatchException("Nil[T]:Параметр T должен быть типом!")
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmNil(ty.substInType(substTerm,i))

  override def toString:String = "Nil["+ty+"]"

  override def isval(boundLevel: Int): Boolean = true
}
