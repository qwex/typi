package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:27
* To change this template use File | Settings | File Templates.
*/
case class TmPred(t:Term) extends Numeral {
  override def eval1(env:List[Term], boundLevel:Int) = t match {
    case TmNat(i) => if (i==0) TmNat(0) else
      TmNat(i-1)
    case TmSucc(t1) if t1.isnumeral =>
      t1
    case _ => throw
      new NoRuleApplies((TmPred(t.eval(env,boundLevel))))
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmPred(t.subst(substTerm,i))

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    if (t.typeof1(ctx,env,boundLevel) == TyNat)
      TyNat
    else
      throw new TypeMismatchException("Ошибка pred: аргумент должен иметь тип Nat!")
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TmPred(t.indexShift(shift,shiftBound))
}
