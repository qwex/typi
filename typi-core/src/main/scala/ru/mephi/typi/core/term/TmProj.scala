package ru.mephi.typi.core.term

import ru.mephi.typi.core.{Binding, Name}
import ru.mephi.typi.core.exception.TypeMismatchException

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 06.09.13
 * Time: 13:50
 * To change this template use File | Settings | File Templates.
 */
case class TmProj(t:Term,proj:String) extends Term{
  override def eval1(env:List[Term],boundLevel:Int) = t match {
    case v@TmRecord(fields) if v.isval(boundLevel) =>
      fields(proj)
    case _ =>
      TmProj(t.eval1(env,boundLevel),proj)
    //case _ => throw new NoRuleApplies(this)
  }

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    t.typeof1(ctx,env,boundLevel) match {
      case TyRecord(types) =>
        if (types.contains(proj))
          types(proj)
        else
          throw new TypeMismatchException("Ошибка ._"+proj+": запись не содержит поле "+proj)
      case _ => throw new TypeMismatchException("Ошибка ._"+proj+": проекция применима только к записи!")
    }
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmProj(t.subst(substTerm,i),proj)

  override def toString = t+"._"+proj
}
