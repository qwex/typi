package ru.mephi.typi.core.term

import ru.mephi.typi.core.{Binding, Name}
import ru.mephi.typi.core.exception.NoRuleApplies

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 06.09.13
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
case class TmRecord(fields:Map[String,Term]) extends Term {
  override def eval1(env:List[Term],boundLevel:Int):Term = {
    def evalrecord(fields:List[(String,Term)]):List[(String,Term)] = fields match {
      case Nil => throw new NoRuleApplies(this)
      case (l,v)::xs if v.isval(boundLevel) => (l,v)::evalrecord(xs)
      case (l,t1)::xs => ((l,t1.eval1(env,boundLevel))::xs)
    }
    TmRecord(evalrecord(fields.toList).toMap)
  }

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    TyRecord(fields.mapValues(t => t.typeof1(ctx,env,boundLevel)))
  }

  override def indexShift(shift:Int ,shiftBound:Int):Term =
    TmRecord(fields.mapValues(t => t.indexShift(shift, shiftBound)))

  override def isval(boundLevel: Int): Boolean =
    fields.foldLeft(true)((acc,field)=> acc &&  field._2.isval(boundLevel))
    //for ((_,t)<-fields if !t.isval(boundLevel)) return false; true

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmRecord(fields.mapValues(t => t.subst(substTerm,i)))

  override def toString = toString1("{",fields.toList)

  def toString1(acc:String,rest:List[(String,Term)]):String = rest match {
    case x::y::xs => toString1(acc+x._1+" = "+x._2+", ",y::xs)
    case y::Nil => acc + y._1 + " = " + y._2+"}"
  }
}
