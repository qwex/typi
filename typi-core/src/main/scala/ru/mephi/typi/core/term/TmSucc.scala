package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:27
* To change this template use File | Settings | File Templates.
*/
case class TmSucc(t:Term) extends Numeral {
  override def eval1(env:List[Term],boundLevel:Int) = t match {
    case TmPred(t1) => t1
    case v if v.isnumeral => v match {
      case TmNat(i) => TmNat(i+1)
      case ts @ TmSucc(t1) => TmSucc(ts.eval1(env,boundLevel))
    }
    case v if v.isval(boundLevel) => v match {
      case _ => throw new NoRuleApplies(this)
    }
    case _ =>
      TmSucc(t.eval(env,boundLevel)).eval(env,boundLevel)
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmSucc(t.subst(substTerm,i))

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    if (t.typeof1(ctx,env,boundLevel) == TyNat)
      TyNat
    else
      throw new TypeMismatchException("Ошибка succ: аргумент должен иметь тип Nat!")
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TmSucc(t.indexShift(shift,shiftBound))

  override def isnumeral: Boolean = t.isnumeral

  override def isval(boundLevel: Int): Boolean = t.isval(boundLevel)
}
