package ru.mephi.typi.core.term

import ru.mephi.typi.core._

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:35
* To change this template use File | Settings | File Templates.
*/
case object TmUnit extends Const {
  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term =
    TyUnit

  override def toString() = "unit"
}
