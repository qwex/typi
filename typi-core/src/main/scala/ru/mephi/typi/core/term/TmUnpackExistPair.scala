package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}
import ru.mephi.typi.core.Index
import ru.mephi.typi.core.VarBind

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 25.11.13
 * Time: 14:34
 * To change this template use File | Settings | File Templates.
 */
case class TmUnpackExistPair(pattern:(String, String), t1:Term, t2:Term) extends Term {
  override def eval1(env:List[Term], boundLevel:Int):Term = t1 match {
      case TmExistPair(t21, t22, ann) =>
        if (t21.isval(boundLevel))
          t2.subst(t22,boundLevel+1).subst(t21,boundLevel)
        else
          TmUnpackExistPair(pattern, t1.eval1(env, boundLevel), t2)
      case _ => throw new NoRuleApplies(this)
  }

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    val tyT1 = t1.typeof(ctx, env, boundLevel)
    tyT1 match {
      case TySigma(_, t31, t32) =>
        t2.typeof1((Index(0, pattern._1), VarBind(t31))::(Index(0, pattern._2), VarBind(t32))::ctx, env, boundLevel+2)
      case _ => throw new  TypeMismatchException("Ошибка распаковки")
    }
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmUnpackExistPair(pattern, t1.generalSubst(substTerm, i, subst), t2.generalSubst(substTerm, i + 1, subst))

  override def isval(boundLevel:Int) = false

  override def toString() = "let "+"{"+pattern._1+","+pattern._2+"} = "+t1+" in "+t2
}
