package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}
import ru.mephi.typi.core.Index

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:30
* To change this template use File | Settings | File Templates.
*/
case class TmVecElim(ty:Term,m:Term,mz:Term,ms:Term,l:Term,list:Term) extends Term {
  override def eval1(env:List[Term], boundLevel:Int) = (ty,m,mz,ms,l,list) match {
    case (ty,vm,vmz,vms,vk,vl) if List(vm,vmz,vms,vk,vl).map(v=>v.isval(boundLevel)).fold(true)(_ && _) =>
      vl match {
        case TmNil(_) => vmz
        case TmCons(_,n,head,tail) =>
          App(App(App(App(vms,n),head),tail),TmVecElim(ty,vm,vmz,vms,n,tail))
        case _ => throw new NoRuleApplies(this)
      }
    case (ty,vm,vmz,vms,vk,l) if List(vm,vmz,vms,vk).map(v=>v.isval(boundLevel)).fold(true)(_ && _) =>
      TmVecElim(ty,vm,vmz,vms,vk,l.eval1(env,boundLevel))
    case (ty,vm,vmz,vms,k,l) if List(vm,vmz,vms).map(v=>v.isval(boundLevel)).fold(true)(_ && _) =>
      TmVecElim(ty,vm,vmz,vms,k.eval1(env,boundLevel),l)
    case (ty,vm,vmz,ms,k,l) if List(vm,vmz).map(v=>v.isval(boundLevel)).fold(true)(_ && _) =>
      TmVecElim(ty,vm,vmz,ms.eval1(env,boundLevel),k,l)
    case (ty,vm,mz,ms,k,l) if vm.isval(boundLevel) =>
      TmVecElim(ty,vm,mz.eval1(env,boundLevel),ms,k,l)
    case (ty,m,mz,ms,k,l) =>
      TmVecElim(ty,m.eval1(env,boundLevel),mz,ms,k,l)
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TmVecElim(
        ty.indexShift(shift,shiftBound),
         m.indexShift(shift,shiftBound),
        mz.indexShift(shift,shiftBound),
        ms.indexShift(shift,shiftBound),
         l.indexShift(shift,shiftBound),
      list.indexShift(shift,shiftBound)
    )

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TmVecElim(
        ty.subst(substTerm,i),
         m.subst(substTerm,i),
        mz.subst(substTerm,i),
        ms.subst(substTerm,i),
         l.subst(substTerm,i),
      list.subst(substTerm,i)
    )

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
      val tyTy   =   ty.typeof1(ctx,env, boundLevel).evalType1(env,boundLevel)
      val mTy    =    m.typeof1(ctx,env, boundLevel).evalType1(env,boundLevel)
      val mzTy   =   mz.typeof1(ctx,env, boundLevel).evalType1(env,boundLevel)
      val msTy   =   ms.typeof1(ctx,env, boundLevel).evalType1(env,boundLevel)
      val kTy    =    l.typeof1(ctx,env, boundLevel).evalType1(env,boundLevel)
      val listTy = list.typeof1(ctx,env, boundLevel).evalType1(env,boundLevel)

      if (
        tyTy == Star &&
          mTy == TyPi("k",TyNat,TyPi("k",TyVec(ty.indexShift(1,0),Var(Index(0,"k"))),Star)) &&
          {/*println("mzTy = " + mzTy);*/mzTy} == {val a = App(App(m,TmNat(0)),TmNil(ty)).evalType1(env,boundLevel).evalType1(env,boundLevel);/*println("a = "+a)*/a} &&
          msTy ==
            TyPi("l",TyNat,TyPi("x",ty.indexShift(1,0),TyPi("xs",TyVec(ty.indexShift(2,0),Var(Index(1,"l"))),TyArr((App(App(m.indexShift(3,0),Var(Index(2,"l"))),Var(Index(0,"xs")))),App(App(m.indexShift(4,0),TmSucc(Var(Index(3,"l")))),TmCons(ty,Var(Index(3,"l")),Var(Index(2,"x")),Var(Index(1,"xs")))))))).
            evalType1(env,boundLevel).evalType1(env, boundLevel) &&
          kTy == TyNat &&
          listTy == TyVec(ty,l)
      )
        App(App(m,l),list).eval(env,boundLevel)
      //evalType1(eval(App(App(m,k),list),env,boundLevel),env,boundLevel)}
      else
        throw new TypeMismatchException("Ошибка типизации VecElim")
  }

  override def toString() =
    "TmVecElim(\n" +
      "  "+ty+",\n"+
      "  "+m+",\n" +
      "  "+mz+",\n"+
      "  "+ms+",\n"+
      "  "+l+",\n" +
      "  "+list+"\n" +
    ")"
}
