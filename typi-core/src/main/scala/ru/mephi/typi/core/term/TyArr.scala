package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 16:31
 * To change this template use File | Settings | File Templates.
 */
case class TyArr(ty1:Term,ty2:Term) extends Type{
  //TODO:Здесь происхоит вычисление именно типа, надо над этим подумать
  override def eval1(env:List[Term],boundLevel:Int):Term =
    throw new NoRuleApplies(TyArr(ty1.evalType1(env,boundLevel),ty2.evalType1(env,boundLevel+1)))

  override def evalType1(env:List[Term], boundLevel:Int) =
    TyArr(ty1.evalType1(env,boundLevel),ty2.evalType1(env,boundLevel+1))
    //eval1(env, boundLevel)

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    if (ty1.typeof1(ctx,env,boundLevel) == Star && ty2.typeof1(null::ctx,env,boundLevel) == Star)
      Star
    else
      throw new TypeMismatchException("Ошибка TyArr")
  }

  override def substInType(substTerm:Term, i:Int):Term =
    TyArr(ty1.substInType(substTerm,i),ty2.substInType(substTerm.indexShift(1,0),i+1))

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TyArr(ty1.substInType(substTerm,i),ty2.substInType(substTerm.indexShift(1,0),i+1))

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TyArr(ty1.indexShift(shift,shiftBound),ty2.indexShift(shift,shiftBound+1))


  override def isval(boundLevel:Int) = ty1.isval(boundLevel) && ty2.isval(boundLevel)//throw new NotImplementedError()

  override def toString() = "("+ty1+"->"+ty2+")"
}
