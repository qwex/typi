package ru.mephi.typi.core.term

import ru.mephi.typi.core.Const

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:31
* To change this template use File | Settings | File Templates.
*/
case object TyBool extends Const with Type  {
  override def toString() = "Bool"
}
