package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}
import ru.mephi.typi.core.Index
import ru.mephi.typi.core.VarBind

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:31
* To change this template use File | Settings | File Templates.
*/
case class TyPi(name:String,ty1:Term,ty2:Term) extends Type  {
  override def eval1(env:List[Term], boundLevel:Int):Term = {
      throw new NoRuleApplies(TyPi(name,ty1.evalType1(env,boundLevel),ty2.evalType1(env,boundLevel+1)))
  }

  override def evalType1(env:List[Term], boundLevel:Int) =
    TyPi(name,ty1.evalType1(env,boundLevel),ty2.evalType1(env,boundLevel+1))
    //eval1(env, boundLevel)

  override def equals(other: Any) = other match {
    case TyPi(_,ty12,ty22) =>  ty1 == ty12 && ty2 == ty22
    case _ => false
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TyPi(name, ty1.indexShift(shift,shiftBound), ty2.indexShift(shift,shiftBound+1))

  override def substInType(substTerm:Term, i:Int):Term =
    TyPi(name,ty1.substInType(substTerm,i),ty2.substInType(substTerm.indexShift(1,0),i+1))

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TyPi(name,ty1.substInType(substTerm,i),ty2.substInType(substTerm.indexShift(1,0),i+1))


  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    if (ty1.typeof1(ctx,env,boundLevel) == Star && ty2.typeof1((Index(0,name),VarBind(ty1))::ctx,env,boundLevel) == Star)
      Star
    else
      throw new TypeMismatchException("Ошибка Pi")
  }

  override def hashCode = 41 * (41 + ty1.hashCode()) + ty2.hashCode()

  override def toString() =
    "(Pi "+name+":"+ty1+"."+ty2+")"
}
