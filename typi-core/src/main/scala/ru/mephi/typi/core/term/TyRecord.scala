package ru.mephi.typi.core.term


/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:32
* To change this template use File | Settings | File Templates.
*/
case class TyRecord(types:Map[String,Term]) extends Type {
  override def substInType(substTerm:Term, i:Int):Term =
    TyRecord(types.mapValues(t => t.substInType(substTerm,i)))

   override def toString = toString1("{",types.toList)
   def toString1(acc:String,rest:List[(String,Term)]):String = rest match {
     case (l,ty)::Nil => acc + l + " : " + ty + "}"
     case (l,ty)::xs => toString1(acc+l+" : "+ty+", ",xs)
   }
}


