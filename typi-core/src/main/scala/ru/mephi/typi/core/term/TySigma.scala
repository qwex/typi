package ru.mephi.typi.core.term

import ru.mephi.typi.core._
import exception.{TypeMismatchException, NoRuleApplies}
import ru.mephi.typi.core.Index
import ru.mephi.typi.core.VarBind

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 11.10.13
 * Time: 13:43
 * To change this template use File | Settings | File Templates.
 */
case class TySigma(name:String, t1:Term, t2:Term) extends Type {
  override def eval1(env:List[Term], boundLevel:Int):Term = {
    throw new NoRuleApplies(TySigma(name, t1.evalType1(env,boundLevel), t2.evalType1(env,boundLevel+1)))
  }

  override def evalType1(env:List[Term], boundLevel:Int) =
    TySigma(name, t1.evalType1(env,boundLevel),t2.evalType1(env,boundLevel+1))
    //eval1(env, boundLevel)

  override def equals(other: Any) = other match {
    case TySigma(_,ty12,ty22) =>  t1 == ty12 && t2 == ty22
    case _ => false
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TySigma(name, t1.indexShift(shift,shiftBound), t2.indexShift(shift,shiftBound+1))

  override def substInType(substTerm:Term, i:Int):Term =
    TySigma(name,t1.substInType(substTerm,i),t2.substInType(substTerm.indexShift(1,0),i+1))

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TySigma(name,t1.substInType(substTerm,i),t2.substInType(substTerm.indexShift(1,0),i+1))


  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    if (t1.typeof1(ctx,env,boundLevel) == Star && t2.typeof1((Index(0,name),VarBind(t1))::ctx,env,boundLevel) == Star)
      Star
    else
      throw new TypeMismatchException("Ошибка Sigma")
  }

  override def hashCode = 41 * (41 + t1.hashCode()) + t2.hashCode()

  override def toString() = "(Si "+name+":"+t1+"."+t2+")"
}

