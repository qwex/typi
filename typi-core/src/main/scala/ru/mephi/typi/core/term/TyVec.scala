package ru.mephi.typi.core.term

import ru.mephi.typi.core.{Binding, Name}
import ru.mephi.typi.core.exception.{TypeMismatchException, NoRuleApplies}

/**
* Created with IntelliJ IDEA.
* User: Александр
* Date: 23.07.13
* Time: 14:28
* To change this template use File | Settings | File Templates.
*/
case class TyVec(ty:Term, n:Term) extends Type {
  override def eval1(env:List[Term], boundLevel:Int):Term = {
    throw new NoRuleApplies(TyVec(ty.evalType1(env, boundLevel), n.eval(env, boundLevel)))
  }

  override def evalType1(env:List[Term], boundLevel:Int) =
    TyVec(ty.evalType1(env,boundLevel),n.evalType1(env,boundLevel))
  //eval1(env, boundLevel)

  override def generalSubst(substTerm:Term, i:Int, subst:(Term, Int) => Term):Term =
    TyVec(ty.substInType(substTerm,i),n.subst(substTerm,i))

  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term = {
    val tyTy = ty.typeof1(ctx, env, boundLevel)
    val tyN  =  n.typeof1(ctx, env, boundLevel)
    if (tyTy == Star && tyN == TyNat)
      Star
    else
      throw new TypeMismatchException("Ошибка Vec")
  }

  override def indexShift(shift:Int, shiftBound:Int):Term =
    TyVec(ty.indexShift(shift,shiftBound), n.indexShift(shift,shiftBound))

  override def toString() =
    "Vec( "+ty+", "+n+")"
}
