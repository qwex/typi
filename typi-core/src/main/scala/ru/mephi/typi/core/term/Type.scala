package ru.mephi.typi.core.term

import ru.mephi.typi.core.{Binding, Name}

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 09.09.13
 * Time: 16:50
 * To change this template use File | Settings | File Templates.
 */
trait Type extends Term {
  override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int):Term =
    Star

  override def isval(boundLevel: Int): Boolean = true
}
