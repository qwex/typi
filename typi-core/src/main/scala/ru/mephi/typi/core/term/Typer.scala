package ru.mephi.typi.core.term

import ru.mephi.typi.core.{VarBind, Index, Binding, Name}
import ru.mephi.typi.core.exception.TypeMismatchException

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 11.04.14
 * Time: 15:44
 * To change this template use File | Settings | File Templates.
 */
object Typer {
  implicit def getResultType(dt:DerivationTree):Term = dt.corollary.ty


  def typeof1(t:Term, ctx:List[(Name,Binding)],env:List[Term],boundLevel:Int):DerivationTree = t match {
    case App(t1,t2) =>
      val tyT2/*:DerivationTree*/ =
        typeof1(t2, ctx, env, boundLevel).evalType(env,boundLevel)
      val tyT1/*:DerivationTree*/ =
        typeof1(t1, ctx, env, boundLevel)

      tyT1.resultType match {
        case TyArr(tyT11, tyT12) =>
          val tyT11e = tyT11.evalType1(env, boundLevel)
          if (tyT11e == tyT2)
            DerivationTree("T-App", List(tyT1,tyT2), Corollary(ctx, t, tyT12.indexShift(-1,0)))
          else
          {
            val subst = tyT11.evalType1(env,boundLevel).unify(tyT2, boundLevel)
            DerivationTree("T-App", List(tyT1, tyT2), Corollary(ctx, t, subst.indexShift(1)(tyT12).indexShift(-1,0)))
          }
        case TyPi(name,tyT11,tyT12) if tyT11.evalType1(env,boundLevel) == tyT2.resultType =>
          //val ty11e = tyT11.evalType1(env, boundLevel)
          val rr = tyT12.substInType(t2.indexShift(1,0),0)
          val r = rr.indexShift(-1,0)
          DerivationTree("T-PiApp" ,List(tyT1, tyT2), Corollary(ctx,t,r.eval(env,boundLevel)))
        case _ =>
          throw new TypeMismatchException("Ошибка App: применяемый терм должен иметь функциональный тип, а тип формального параметра должен совпадать с типом аргумента!")
      }

    case Lambda(body, ty, name) =>
      val tyTy = typeof1(ty ,ctx, env, boundLevel)
      if (tyTy.resultType == Star)
      {
        val eTy = ty.evalType1(env, boundLevel)
        val tyBody = typeof1(body, (Index(0,name), VarBind(eTy))::ctx, env, boundLevel+1)
        DerivationTree("T-Lambda", List(tyTy ,tyBody), Corollary(ctx, t, TyArr(eTy, tyBody)))
      }
      else
        throw new TypeMismatchException("Ошибка Lambda: "+ ty + " не является типом!")
    case TLambda(body, ty, name) =>
      val tyTy = typeof1(ty ,ctx, env, boundLevel)
      if (tyTy.resultType == Star)
      {
        val eTy = ty.evalType1(env, boundLevel)
        val tyBody = typeof1(body, (Index(0,name), VarBind(eTy))::ctx, env, boundLevel+1)
        DerivationTree("T-TLambda", List(tyTy, tyBody), Corollary(ctx, t, TyPi(name, ty, tyBody)))
      }
      else
        throw new TypeMismatchException("Ошикба TLambda: "+ ty + " не является типом!")
    case Let(name, t1, t2) =>
      val tyT1 = typeof1(t1, ctx,env,boundLevel)
      val tyT2 = typeof1(t2 ,(Index(0,name),VarBind(tyT1))::ctx,env/*++List(t1)*/,boundLevel/*+1*/)
      DerivationTree("T-Let", List(tyT1, tyT2), Corollary(ctx, t, tyT2))
    case Star =>
      DerivationTree("T-Star", Nil, Corollary(ctx, t, Star))
    case bool : Bool =>
      DerivationTree("T-Bool", Nil , Corollary(ctx, t, TyBool))
    case TmIf(cond, t1, t2) =>
      val tyCond = typeof1(cond, ctx,env, boundLevel)
      if (TyBool == tyCond.resultType)
      {
        val tyT1 = typeof1(t1, ctx, env, boundLevel)
        val tyT2 = typeof1(t2, ctx, env, boundLevel)

        if (tyT1.resultType == tyT2.resultType)
          DerivationTree("T-If", List(tyCond, tyT1, tyT2), Corollary(ctx, t, tyT1))
        else
          throw new TypeMismatchException("Ошибка if: обе альтернативы должны иметь один и тот же тип!")
      }
      else
        throw new TypeMismatchException("Ошибка if: условие должно иметь тип Bool")
    case TmIsZero(n) =>
      val tyN = typeof1(n, ctx, env, boundLevel)
      if (tyN.resultType == TyNat)
        DerivationTree("T-IsZero", List(tyN), Corollary(ctx, t, TyBool))
      else
        throw new TypeMismatchException("Ошибка isZero: аргумент должен иметь тип Nat!")
    case TmPred(n) =>
      val tyN = typeof1(n, ctx, env, boundLevel)
      if (tyN.resultType == TyNat)
        DerivationTree("T-IsZero", List(tyN), Corollary(ctx, t, TyNat))
      else
        throw new TypeMismatchException("Ошибка pred: аргумент должен иметь тип Nat!")
    case TmSucc(n) =>
      val tyN = typeof1(n, ctx, env, boundLevel)
      if (tyN.resultType == TyNat)
        DerivationTree("T-IsZero", List(tyN), Corollary(ctx, t, TyNat))
      else
        throw new TypeMismatchException("Ошибка succ: аргумент должен иметь тип Nat!")
    case TmNat(i) =>
      DerivationTree("T-Nat", Nil, Corollary(ctx, t, TyNat))
    case TmUnit =>
      DerivationTree("T-Unit", Nil, Corollary(ctx, t, TyUnit))
    case TyArr(ty1, ty2) =>
      val tyTy1 = typeof1(ty1, ctx, env, boundLevel)
      val tyTy2 = typeof1(ty2, null::ctx, env, boundLevel)
      if (tyTy1.resultType == Star && tyTy2.resultType == Star)
        DerivationTree("T-Arr", List(tyTy1, tyTy2), Corollary(ctx, t, Star))
      else
        throw new TypeMismatchException("Ошибка TyArr")
    case TyPi(name, ty1, ty2) =>
      val tyTy1 = typeof1(ty1, ctx, env, boundLevel)
      val tyTy2 = typeof1(ty2, (Index(0,name),VarBind(ty1))::ctx, env, boundLevel)
      if (tyTy1.resultType == Star && tyTy2.resultType == Star)
        DerivationTree("T-Pi", List(tyTy1, tyTy2), Corollary(ctx, t, Star))
      else
        throw new TypeMismatchException("Ошибка Pi")
    case ty : Type =>
      DerivationTree("T-Type", Nil, Corollary(ctx, t, Star))
    case Var(name) =>
      DerivationTree("T-Var", Nil, Corollary(ctx, t, name.getTypeFromContext(ctx)))
    case TmRecord(fields) =>
      val assumptions = fields.mapValues(t => typeof1(t, ctx, env, boundLevel))
      DerivationTree("T-Record", assumptions.values.toList, Corollary(ctx, t, TyRecord(assumptions.mapValues(dt=>dt.resultType))))
      //TyRecord(fields.mapValues(t => t.typeof1(ctx,env,boundLevel)))
    //case TmProj(t:Term, proj:String) =
    case UnknownType(i,name) =>
      DerivationTree("T-UnknownType", Nil, Corollary(ctx, t, Star))
    case Fix(t) =>
      val tTy = typeof1(t, ctx, env, boundLevel)
      tTy.resultType match {
        case TyArr(t21,t22) if t21 == t22 =>
          DerivationTree("T-Fix", List(tTy), Corollary(ctx, t, t21))
        case _ =>
          throw new TypeMismatchException("Ошибка Fix")
      }
    case _ =>
      throw new TypeMismatchException("Ошибка типиации: неизвестный тип " + t)
  }
}

case class DerivationTree(ruleName:String, assumptions:List[DerivationTree], corollary:Corollary) {
  //override def toString = "\\dfrac{"+assumptions.mkString("\\,\\,\\,")+"}{"+corollary+"}(\\text{"+ruleName+"})"
  //override def toString = ""
  def resultType = corollary.ty
  def evalType(env:List[Term], boundLevel:Int) =
    DerivationTree(ruleName, assumptions, Corollary(corollary.ctx, corollary.t, corollary.ty.evalType1(env,boundLevel)))
}



case class Corollary(ctx:List[(Name, Binding)], t:Term, ty:Term) {
  /*override def toString = "\\text{"+/*ctx.map((p)=>
    if (p != null)
      p._1+" = "+p._2.toString
    else
      " "
  ).mkString(", ")*/{if (ctx.length > 0) ctx.head+"Г" else ""}+"} \\vdash \\text{"+t+" : } \\text{"+ty+"}" */
  override def toString = {if (ctx.length > 0) ctx.head+"Г" else ""}+"├"+t+" : "+ty
}

case object Printer
{
  def apply(binding:(Name,Binding)) = if (binding == null) "null" else binding._1+" = "+binding._2
  def apply(term:Term):String = term match {
    case TLambda(body, ty, name) => "<b>Λ</b>"+name+":"+Printer(ty)+"."+Printer(body)
      //"\\(\\Lambda\\)"+name+":"+Printer(ty)+"."+Printer(body)
    case TyArr(ty1, ty2) => ty1 match {
      case TyArr(_,_) | TyPi(_,_,_) =>
        "("+Printer(ty1)+")"+"→"+Printer(ty2)//"("+Printer(ty1)+")"+"\\(\\rightarrow\\)"+Printer(ty2)
      case _ =>
        Printer(ty1)+"→"+Printer(ty2)
        //Printer(ty1)+"\\(\\rightarrow\\)"+Printer(ty2)
    }
    case Lambda(body, ty, name) =>
      "<b>λ</b>"+name+":"+Printer(ty)+"."+Printer(body)
      //"\\(\\lambda\\)"+name+":"+Printer(ty)+"."+Printer(body)
    case App(t1,t2) =>
      {t1 match {
        case TLambda(_,_,_) | Lambda (_,_,_) =>
          "("+Printer(t1)+")"
        case _ => Printer(t1)
      }} + " " + {t2 match {
        case App(_,_) => "("+Printer(t2)+")"
        case _ => Printer(t2)
      }}
    case TyPi(name, ty1, ty2) =>
      "<b>Π</b>"+name+":"+Printer(ty1)+"."+Printer(ty2)
      //"\\(\\Pi\\) "+name+":"+Printer(ty1)+"."+Printer(ty2)
    case Var(name) => name match {
      case Index(i, n) => n
      case _=> term.toString
    }
    case _ => term.toString
  }
  def apply(derivationTree:DerivationTree):String =
    "<div class=\"rule\">" +
      "<div style=\"position: relative\">" +
        "<div class=\"plus\">+</div>" +
        "<div style=\"float:left;\">&nbsp;</div>" +
        "<div class=\"antecedent\">"+derivationTree.assumptions.map(assumption=>this(assumption)).mkString(" ")+"</div>" +
        "<div style=\"clear:both\"></div>" +
      "</div>" +
      "<div class=\"name\">("+derivationTree.ruleName+")</div>" +
      "<div class=\"consequent\">"+this(derivationTree.corollary)+"</div>" +
    "</div>"
  def apply(corollary:Corollary):String =
    {
      if (corollary.ctx.length > 0) "Г, "+Printer(corollary.ctx.head) else ""
    }+"⊢"+Printer(corollary.t)+" :: "+Printer(corollary.ty)
      //" \\(\\vdash\\) "+Printer(corollary.t)+" :: "+Printer(corollary.ty)
}
