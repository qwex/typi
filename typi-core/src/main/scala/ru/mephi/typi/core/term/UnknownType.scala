package ru.mephi.typi.core.term

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 10.03.14
 * Time: 1:14
 * To change this template use File | Settings | File Templates.
 */
case class  UnknownType(index:Int, name:String) extends Type {
  //var name:String = ""
  override def toString() = name+"?("+index+")"
  def this(index:Int) { this(index, "") }

  override def isval(boundLevel:Int) =
    true


  /*override def indexShift(shift:Int, shiftBound:Int) =
    //if (index>=shiftBound)
      UnknownType(index+shift,name)
    //else
    //  this*/
  //override def substInType(substTerm:Term, i:Int):Term = {
  //
  //} */
}
