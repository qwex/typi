package ru.mephi.typi.core.term

import ru.mephi.typi.core.{Literal, Binding, Index, Name}
import ru.mephi.typi.core.exception.NoRuleApplies

/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 02.09.13
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */
case class Var(name:Name) extends Term {
    //Интерфейс
    override def eval1(env:List[Term],boundLevel:Int) = name match {
      case ii@Index(i,_) if !isval(boundLevel) =>
        println("Имя переменной: "+name, "Глубина связывания "+boundLevel)
       (env(i-boundLevel)).indexShift(i+1,/*boundLevel*/0)
      case _ => throw new NoRuleApplies(this)
    }

    override def typeof1(ctx:List[(Name,Binding)], env:List[Term], boundLevel:Int) =
      name.getTypeFromContext(ctx)

  override def isval(boundLevel:Int):Boolean = name match {
    case Literal(_) => true
    case Index(i,_) => i<boundLevel
  }

    override def indexShift(shift:Int, shiftBound:Int):Term = name match {
      case Index(i,n) if i>= shiftBound =>
        Var(Index(i+shift,n))
      case  _ => this
    }




  override def substInType(substTerm: Term, i: Int): Term = {
    name match {
    case Index(j,/*_*/name) if i == j =>
      substTerm match {
        case UnknownType(n,name2) => val r = UnknownType(j,name);r
        case _ => substTerm
      }
    case _ => Var(name)}
  }

  override def generalSubst(substTerm:Term, i:Int, subst:(Term,Int)=>Term):Term = name match {
    case Index(j,_) if i == j => substTerm
    case _ => Var(name)
  }

  override def toString() = name.toString
    //Cобственные методы
}

