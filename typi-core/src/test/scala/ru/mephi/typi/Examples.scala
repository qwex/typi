package ru.mephi.typi

import org.specs2.mutable._
import ru.mephi.typi.core.term._


/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 31.01.14
 * Time: 17:43
 * To change this template use File | Settings | File Templates.
 */
class Examples extends Specification {
  val parser = new LangParser
  val m = parser.MakeTree("Lambda x:Nat.if iszero(x) then Nat else Unit)")
  val mz = parser.MakeTree("10")
  val ms = parser.MakeTree("TLambda x:Nat.Lambda y:(if iszero(x) then Nat else Unit).unit")

  "NatElim("+m+","+mz+","+ms+", 0).eval(Nil) must be equal 10" in {
    TmNatElim(m,mz,ms,TmNat(0)).eval(Nil) must beEqualTo(TmNat(10))
  }

  "NatElim("+m+","+mz+","+ms+", 1).eval(Nil) must be equal Unit" in {
    TmNatElim(m,mz,ms,TmNat(1)).eval(Nil) must beEqualTo(TmUnit)
  }


}
