package ru.mephi.typi

import org.specs2.mutable._
import ru.mephi.typi.core.term._
import ru.mephi.typi.core._
import scala.util.parsing.combinator._
/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 29.01.14
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */

class LangParserTest extends Specification {
  //def is = example
  val parser = new LangParser
  lazy val parseResult1 = parser.MakeTree("x")
  lazy val parseResult2 = parser.MakeTree("Lambda x:Nat.x")
  lazy val parseResult3 = parser.MakeTree("(Nat->Unit)->Nat")
  lazy val lexResult1 = parser.parse(parser.Lex, "a; /* bbbdf Lambda x.:Nat.y */ b").get
  lazy val lexResult2 = parser.parse(parser.Lex, "a; b").get
  lazy val lexResult3 = parser.parse(parser.Lex, "TLambda x:*.x").get
  //def example =  parser.MakeTree("x") must beEqualTo(Var(Literal("x")))
  //"With LangParser" should {
    "MakeTree(\"x\") must be equal to Var(Literal(\"x\"))" in {
      parseResult1 must beEqualTo(Var(Literal("x")))
    }
    "MakeTree(\"Lambda x:Nat.x\") must be equal to Lambda(Var(Index(0,\"x\"),\"x\",TyNat)" in {
      parseResult2 must beEqualTo(Lambda(Var(Index(0,"x")),TyNat,"x"))
    }

    "MakeTree(\"(Nat->Unit)->Nat\") must be equal to TyArr(TyArr(TyNat,TyUnit),TyNat)" in {
      parseResult3 must beEqualTo(TyArr(TyArr(TyNat,TyUnit),TyNat))
    }

    "Lex of \"a; /* bbbdf Lambda x.:Nat.y */b\" must be equal to \"##a;##b\"" in {
      lexResult1 must beEqualTo("##a;##b")
    }


  "Lex of \"a; b\" must be equal to \"##a;##b\"" in {
    lexResult2 must beEqualTo("##a;##b")
  }

  "Lex of \"Lambda x:*.x\" must be equal to \"#TLambda ##x:*.##x\"" in {
    lexResult3 must beEqualTo("#TLambda ##x:*.##x")
  }
}

