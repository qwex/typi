package ru.mephi.typi

import core.VarBind
import org.specs2.mutable._
import ru.mephi.typi.core.term._
import ru.mephi.typi.core._

class ProcessSamples extends Specification {

  //val parser = new LangParser

  "Describe simple process" in {
    val context = List(
      //Worker : *
      Index(0, "Worker") -> VarBind(Star),
      //SkilledWorker : *
      Index(0, "SkilledWorker") -> VarBind(Star),
      //SkilledEnrollment : *
      Index(0, "SkilledEnrollment") -> VarBind(Star),
      //badSource : Unit->Worker
      Index(0, "badSource") -> VarBind(TyArr(TyUnit, Var(Index(3,"Worker")))),
      // interview : (Unit->Worker)->(Unit->SkilledWorker)
      Index(0, "interview") -> VarBind(TyArr(TyArr(TyUnit,Var(Index(4,"Worker"))), TyArr(TyUnit, Var(Index(4,"SkilledWorker"))))),
      Index(0, "skilledEnroll") -> VarBind(TyArr(Var(Index(3,"SkilledWorker")), Var(Index(3, "SkilledEnrollment"))))
    ).reverse
    //Goal = Unit->SkilledEnrollment
    val Goal = TyArr(TyUnit, Var(Index(4, "SkilledEnrollment")))
    // longSolutinon = Lambda x:Unit.skilledEnrol ((inerview badSource) x)
    val longSolution = Lambda(App(Var(Index(1, "skilledEnroll")), App(App(Var(Index(2, "interview")), Var(Index(3, "badSource"))), Var(Index(0, "x")))), TyUnit, "x")
    longSolution.typeof1(context, List(), 6)   must beEqualTo(Goal)
  }

  "Describe complex process" in {
    val SkilledWorker = TySigma("worker", Var(Index(1,"Worker")), App(Var(Index(1, "Skill")), Var(Index(0, "worker"))))
    val context = List(
      //Worker : *
      Index(0, "Worker") -> VarBind(Star),
      //SkilledWorker : *
      Index(0, "Skill") -> VarBind(TyArr(Var(Index(0, "Worker")) ,Star)),
      Index(0, "SkilledWorker") -> VarBind(Star),
      //SkilledEnrollment : *
      Index(0, "SkilledEnrollment") -> VarBind(Star),
      //badSource : Unit->Worker
      Index(0, "badSource") -> VarBind(TyArr(TyUnit, Var(Index(4,"Worker")))),
      // interview : (Unit->Worker)->(Unit->SkilledWorker)
      Index(0, "interview") -> VarBind(TyArr(TyArr(TyUnit,Var(Index(5,"Worker"))), TyArr(TyUnit, SkilledWorker.indexShift(4,0)))),
      Index(0, "skilledEnroll") -> VarBind(TyArr(SkilledWorker.indexShift(3, 0), Var(Index(3, "SkilledEnrollment"))))
    ).reverse
    //Goal = Unit->SkilledEnrollment
    val Goal = TyArr(TyUnit, Var(Index(4, "SkilledEnrollment")))
    //val longSolution = App(Var(Index(1, "interview")), Var(Index(2,"badSource")))
    //longSolutinon = Lambda x:Unit.skilledEnrol ((inerview badSource) x)
    val longSolution = Lambda(App(Var(Index(1, "skilledEnroll")), App(App(Var(Index(2, "interview")), Var(Index(3, "badSource"))), Var(Index(0, "x")))), TyUnit, "x")
    longSolution.typeof1(context, List(), 7)   must beEqualTo(Goal)
  }
}
