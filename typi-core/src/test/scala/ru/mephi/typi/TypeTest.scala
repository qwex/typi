package ru.mephi.typi

import org.specs2.mutable._
import ru.mephi.typi.core.term._
import ru.mephi.typi.core._
/**
 * Created with IntelliJ IDEA.
 * User: Александр
 * Date: 15.06.14
 * Time: 4:15
 * To change this template use File | Settings | File Templates.
 */
class TypeTest extends Specification {
  def parser = new LangParser
  val consts = List(
    (parser.MakeTree("unit"), TyUnit),
    (parser.MakeTree("0"), TyNat),
    (parser.MakeTree("100"), TyNat),
    (parser.MakeTree("false"), TyBool),
    (parser.MakeTree("true"), TyBool),
    (parser.MakeTree("Unit"), Star),
    (parser.MakeTree("Bool"), Star),
    (parser.MakeTree("Nat"), Star)
  )

  for (const<-consts) testTypeOfConst(const._1, const._2)

  //val input = parser.MakeTree("Lambda x:Nat.x")
  val lambdaTests = List(
    ("Lambda x:Nat.x", "Nat->Nat"),
    ("Lambda x:Nat.Lambda y:Nat.unit", "Nat->(Nat->Unit)"),
    ("Lambda x:Nat->Nat.x ((Lambda y:Nat.y) 10)", "(Nat->Nat)->Nat")
  )

  for (test<-lambdaTests) testType(test._1, test._2)

  def testTypeOfConst(input:Term, ty:Term)
  {
    "typeof("+input+") must be equal to "+ty in {
      input.typeof(Nil) must beEqualTo(ty)
    }
  }

  def testType(inputStr:String, expectedResultStr:String)
  {
    val input = parser.MakeTree(inputStr)
    val expectedResult = parser.MakeTree(expectedResultStr)

    "typeof("+input+") must be equal to "+expectedResult in {
      input.typeof(Nil) must beEqualTo(expectedResult)
    }
  }
}
