package controllers

import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import ru.mephi.typi.{EvalResult, Interpreter}

object Application extends Controller {

  val evalForm = Form(
    "code" -> text
  )

  def index = Action {
    Ok(views.html.index(evalForm fill views.txt.code().toString, List()))
  }

  def eval = Action { implicit request =>
    val form = evalForm.bindFromRequest
    form("code").value.map({ code =>
	    val results : Iterable[EvalResult] = Interpreter.eval(code)
      Ok(views.html.index(form, results))
    }) getOrElse PreconditionFailed(views.html.index(form, List()))
  }

}